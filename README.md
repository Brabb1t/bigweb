# bigweb

计划完成状态

登录管理模块：
1. 权限控制：用户不能访问系统管理模块。     **完成** 
2. 登录错误信息提示。     **完成** 

系统管理模块（web）
1. 登录系统的管理员能够维护用户信息，对用户信息进行增、删、改、查。     **完成** 
2. 维护书籍、电影、音乐信息，对书籍、电影、音乐信息进行增、删、改、查。     **完成** 
3. 维护用户的评价信息，并可以查询指定用户的评价记录。     **完成** 

客户端模块（web）
1. 未登录用户可以查看评价，可以根据书影音的类型、年份等对书影音进行过滤搜索。     **完成** 
2. 用户可以进行注册，登录用户可以为某个书影音编写评价信息。     **100%** 
3. 可以维护自己的个人基本信息与联系方式。     **100%** 
4. 可以查询自己已发表的评价信息，做增、删、改、查。     **完成** 

