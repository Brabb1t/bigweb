package org.npu.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 */
public class DateUtil {

    /**
     * 返回今年开始的时间
     * @return  Date
     */
    public Date getCurrentYearStartTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), Calendar.JANUARY, 1, 0, 0, 0);
        return cal.getTime();
    }

    /**
     * 返回今年结束的时间
     * @return  Date
     */
    public Date getCurrentYearEndTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getCurrentYearStartTime());
        cal.add(Calendar.YEAR, 1);
        cal.add(Calendar.SECOND, -1);
        return cal.getTime();
    }

    /**
     * 返回一个较早的时间
     * @return  Date 1990年
     */
    public Date getEarlyTime() {
        Date result = null;
        try {
            result = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("1990-1-1 00:00:00");
        } catch (ParseException e) {
        }
        return result;
    }

}