package org.npu.util;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.UserDao;
import org.npu.entity.User;

/**
 * 工具类，用来给管理员或者用户修改个人信息
 */
public class UserMesChange {

    SqlSession sqlSession = MyBatisUtils.getSqlSession();

    /**
     * 修改用户信息
     *
     * @param userName    新用户名
     * @param email       新邮箱
     * @param password    新密码
     * @param header      新头像     可以为null
     * @param description 新个人描述   可以为null
     * @return int 1为成功，0为失败
     */
    public int changeUserMes(Integer id, String userName, String email, String password, String header, String description) {

        //数据库操作对象
        UserDao userDao = sqlSession.getMapper(UserDao.class);
        int result = userDao.update(id, userName, email, password, header, description);
        sqlSession.commit();//提交事务

        return result;
    }

    /**
     * 添加用户
     *
     * @param userName 用户名
     * @param email    邮箱
     * @param password 密码
     * @return int 1为成功，0为失败
     */
    public int addUser(String userName, String email, String password) {
        UserDao userDao = sqlSession.getMapper(UserDao.class);
        int result = userDao.Register(userName, email, password);
        sqlSession.commit();
        return result;
    }

}
