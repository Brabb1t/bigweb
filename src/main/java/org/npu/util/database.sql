-- 管理员表
CREATE TABLE admin(
    id integer primary key AUTO_INCREMENT,
    username varchar(20) unique not null,
    password varchar(40) not null,
    email varchar(40) not null
);
-- 用户表
CREATE TABLE user(
    id integer primary key AUTO_INCREMENT,
    username varchar(20) unique not null,
    password varchar(40) not null,
    email varchar(40) not null,
    header varchar(256),
    description varchar(256)
);

-- 作品表，为了方便评论关联，将电影音乐书籍拆分成三张表
CREATE TABLE item(
    id integer primary key AUTO_INCREMENT,
    name varchar(256) not null,
    description varchar(512) not null,
    score integer not null,
    ReleaseDate date not null, -- 出版时间或发行时间
    MainPic varchar(256), -- 首页图片
    DescribePic varchar(256) -- 描述图片
);
-- 电影表
CREATE TABLE movie(
    MovieId integer primary key,
    director varchar(16) not null,
    actor varchar(16) not null, -- 男主
    actress varchar(16) not null, -- 女主
    foreign key (MovieId) references item(id)
);
-- 音乐表
CREATE TABLE music(
    MusicId integer primary key,
    singer varchar(20) not null,
    album varchar(20) not null,
    foreign key (MusicId) references item(id)
);
-- 书籍表
CREATE TABLE book(
    BookId integer primary key,
    author varchar(16) not null,
    publisher varchar(16) not null,
    foreign key (BookId) references item(id)
);

-- 评论表
CREATE TABLE comment(
    id integer primary key AUTO_INCREMENT,
    ItemId integer not null,
    UserId integer not null,
    CommentTime datetime not null, -- 评论时间
    context varchar(512) not null, -- 评论内容
    score integer not null, -- 评分
    checked integer not null, -- 是否审核通过
    foreign key (ItemId) references item(id),
    foreign key (UserId) references user(id)
);