package org.npu.dao;

import org.apache.ibatis.annotations.Param;
import org.npu.entity.Admin;


public interface AdminDao {

    // 管理员登录
    int AdminLogin(@Param("username") String username, @Param("password") String password);

    /**
     * 根据姓名和密码返回管理员
     * @param username  用户名
     * @param password  密码
     * @return  Admin 管理员对象
     */
    Admin queryByNameAndPassword(@Param("username") String username, @Param("password") String password);
}
