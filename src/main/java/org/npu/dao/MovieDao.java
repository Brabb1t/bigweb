package org.npu.dao;

import org.apache.ibatis.annotations.Param;
import org.npu.entity.Movie;

import java.util.Date;
import java.util.List;

public interface MovieDao {

    /**
     * 拿到最新的一部电影
     *
     * @return Movie
     */
    Movie getLatest();

    /**
     * 拿到需要展示电影列表
     *
     * @param startIndex 开始展示的坐标
     * @param pageSize   一页展示的个数
     * @return List
     */
    List<Movie> queryList(@Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据id查找一个对象
     *
     * @param id id值
     * @return Movie对象
     */
    Movie queryById(Integer id);

    /**
     * 查询电影的总数量
     *
     * @return int 电影总数量
     */
    int count();

    /**
     * 根据时间和分数来筛选电影
     *
     * @param startScore 开始分数（小）
     * @param endScore   结束分数（大）
     * @param startTime  开始时间
     * @param endTime    结束时间
     * @return 电影队列
     */
    List<Movie> queryByScoreAndTime(@Param("startScore") Integer startScore, @Param("endScore") Integer endScore,
                                    @Param("startTime") Date startTime, @Param("endTime") Date endTime);


    /**
     * 添加电影 (在movie表中添加)
     * @param itemid 对象id
     * @param director 导演
     * @param actor 男演员
     * @param actress 女演员
     * @return 1为成功，0为失败
     */
    int addMovie(@Param("itemid") String itemid,@Param("director") String director, @Param("actor") String actor,@Param("actress") String actress);

    /**
     * 查询电影(根据电影名)
     * @param moviename 电影名
     * @return 电影
     */
    Movie queryByNameMovie(@Param("moviename") String moviename);

    /**
     * 查询电影(根据电影名)
     * @param movieid 电影id
     * @return 电影
     */
    Movie queryByIdMovie(@Param("movieid") String movieid);

    /**
     * 删除电影(从movie)
     * @param movieid 电影id
     * @return 1为成功，0为失败
     */
    int deleteMovie(@Param("movieid") String movieid);

    /**
     *
     * @param movieid 电影id
     * @param moviename 电影名
     * @param releaseDate 上映日期
     * @param score 评分
     * @param description 描述
     * @param mainPic 海报
     * @param describePic 详细海报
     * @return 1为成功，0为失败
     */
    int updateMovie(@Param("movieid") String movieid,@Param("moviename") String moviename,@Param("director") String director, @Param("actor") String actor,@Param("actress") String actress,@Param("releaseDate") Date releaseDate, @Param("score") double score, @Param("description") String description, @Param("mainPic") String mainPic,@Param("describePic") String describePic);

    /**
     * 查询全部电影
     * @return 电影List
     */
    List<Movie> queryAllMovie();

}
