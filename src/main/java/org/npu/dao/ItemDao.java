package org.npu.dao;

import org.apache.ibatis.annotations.Param;
import org.npu.entity.Item;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: Rehemaiti
 * @Date: 2021/11/10/14:24
 * @Description:
 */
public interface ItemDao {
    /**
     * 获取最新添加item的id
     * @return item
     */
    Item getLastItem();

    /**
     * 在item中添加
     * @param itemname 对象名
     * @param releaseDate 出版日期
     * @param score 评分
     * @param description 描述
     * @param mainPic 海报
     * @param describePic 详细海报
     * @return 1为成功，0为失败
     */
    int addItem(@Param("itemname") String itemname, @Param("releaseDate") Date releaseDate, @Param("score") double score, @Param("description") String description, @Param("mainPic") String mainPic, @Param("describePic") String describePic);

    /**
     * 按照id删除item
     * @param itemid 对象id
     * @return 1为成功，0为失败
     */
    int deleteById(@Param("itemid") String itemid);
}
