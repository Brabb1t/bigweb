package org.npu.dao;

import org.apache.ibatis.annotations.Param;
import org.npu.entity.Movie;
import org.npu.entity.Music;

import java.util.Date;
import java.util.List;

public interface MusicDao {
    /**
     * 获取最新的音乐
     *
     * @return Music
     */
    Music getLatest();

    /**
     * 返回需要展示的音乐对象
     *
     * @param startIndex 开始查询的坐标
     * @param pageSize   查询的个数
     * @return List
     */
    List<Music> queryList(@Param("startIndex") int startIndex, @Param("pageSize") int pageSize);

    /**
     * 根据id值查找对象
     * @param id    id值
     * @return  Music对象
     */
    Music queryById(Integer id);

    /**
     * 查询音乐的总数量
     * @return  int 音乐总数量
     */
    int count();

    /**
     * 根据音乐的分数，时间筛选电影
     * @param startScore    开始分数
     * @param endScore      结束分数
     * @param startTime     开始时间
     * @param endTime       结束时间
     * @return  List    音乐队列
     */
    List<Music> queryByScoreAndTime(@Param("startScore") Integer startScore, @Param("endScore") Integer endScore,
                                    @Param("startTime") Date startTime, @Param("endTime") Date endTime);


    /**
     * 添加音乐(在music表中添加)
     * @param itemid 对象id
     * @param singer 歌手
     * @param album 专辑
     * @return 1为成功，0为失败
     */
    int addMusic(@Param("itemid") String itemid,@Param("singer") String singer, @Param("album") String album);

    /**
     * 查询音乐(根据音乐名)
     * @param musicname 音乐名
     * @return 音乐
     */
    Music queryByNameMusic(@Param("musicname") String musicname);

    /**
     * 查询音乐(根据音乐id)
     * @param musicid 音乐id
     * @return
     */
    Music queryByIdMusic(@Param("musicid") String musicid);

    /**
     * 删除音乐(从music)
     * @param musicid 音乐id
     * @return 1为成功，0为失败
     */
    int deleteMusic(@Param("musicid") String musicid);


    /**
     * 更新音乐(按照id)
     * @param musicid 音乐id
     * @param musicname 音乐名
     * @param singer 歌手
     * @param album 专辑
     * @param releaseDate 发行日期
     * @param score 评分
     * @param description 描述
     * @param mainPic 海报
     * @param describePic 详细海报
     * @return 1为成功，0为失败
     */
    int updateMusic(@Param("musicid") String musicid,@Param("musicname") String musicname,@Param("singer") String singer, @Param("album") String album,@Param("releaseDate") Date releaseDate, @Param("score") double score, @Param("description") String description, @Param("mainPic") String mainPic,@Param("describePic") String describePic);

    /**
     * 查询全部音乐
     * @return 音乐List
     */
    List<Music> queryAllMusic();

}
