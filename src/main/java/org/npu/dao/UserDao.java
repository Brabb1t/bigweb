package org.npu.dao;

import org.apache.ibatis.annotations.Param;
import org.npu.entity.User;

import java.util.List;

public interface UserDao {

    /**
     * 登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @return 1为成功，0为失败
     */
    int Login(@Param("username") String username, @Param("password") String password);

    /**
     * 新用户注册
     *
     * @param username 用户姓名
     * @param email    用户Email
     * @param password 用户密码
     * @return 1为成功，0为失败
     */
    int Register(@Param("username") String username, @Param("email") String email, @Param("password") String password);

    /**
     * 查询(根据id)
     *
     * @param id 用户id
     * @return user 用户全部信息
     */
    User queryById(@Param("id") String id);

    /**
     * 查询(用户名)
     *
     * @param username 用户名
     * @return user 用户全部信息
     */
    User queryByName(@Param("username") String username);

    /**
     * 删除用户
     *
     * @param id 用户id
     * @return int 1为成功，0为失败
     */
    int deleteUser(@Param("id") String id);

    /**
     * 拿到所有用户
     *
     * @return List用户列表
     */
    List<User> getList();

    /**
     * 根据id修改用户信息
     *
     * @return int 1为成功，0为失败
     */
    int update(@Param("id") Integer id, @Param("userName") String userName, @Param("email") String email, @Param("password") String password,
               @Param("header") String header, @Param("description") String description);

  /**
     * 根据id修改用户信息
     *
     * @return int 1为成功，0为失败
     */
    int updateUser(@Param("id") String id, @Param("userName") String userName, @Param("email") String email, @Param("password") String password,
               @Param("header") String header, @Param("description") String description);






    /**
     * 根据用户名和密码来查询用户的id
     * @param username  用户名
     * @param password  密码
     * @return  int 用户的id
     */
    int getId(@Param("username")String username, @Param("password")String password);

}
