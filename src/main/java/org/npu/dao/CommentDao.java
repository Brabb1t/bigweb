package org.npu.dao;

import org.apache.ibatis.annotations.Param;
import org.npu.entity.Comment;

import java.util.List;

public interface CommentDao {

    /**
     * 根据用户用户id选出所有评论
     *
     * @param id 用户id值
     * @return List 评论
     */
    List<Comment> queryById(Integer id);

    /**
     * 添加评论
     *
     * @param comment 评论
     * @return 1为成功，0为失败
     */
    int add(Comment comment);

    /**
     * 根据id和数据库下标选出所有评论
     *
     * @param id id值
     * @return List 评论
     */
    List<Comment> queryByIdWithLimit(@Param("id") Integer id, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据id统计总共有多少条评论
     * @param id    id值
     * @return  int 评论数量
     */
    int count(Integer id);

    /**
     * 根据itemid查询评论
     * @param id    item的id值
     * @return  List评论列表
     */
    List<Comment> queryByItemId(Integer id);

    /**
     * 删除评论（将checked属性改为0）
     * @param id 评论的id值
     * @return  1为成功，0为失败
     */
    int delete(Integer id);

}
