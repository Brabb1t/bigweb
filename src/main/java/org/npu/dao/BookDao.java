package org.npu.dao;

import org.apache.ibatis.annotations.Param;
import org.npu.entity.Book;

import java.util.Date;
import java.util.List;

public interface BookDao {
    /**
     * 获取最新的一本书
     *
     * @return 书籍
     */
    Book getLatest();

    /**
     * 获取需要展示的书籍列表
     *
     * @param startIndex 开始展示的坐标
     * @param pageSize   展示的个数
     * @return List
     */
    List<Book> queryList(@Param("startIndex") int startIndex, @Param("pageSize") int pageSize);

    /**
     * 根据id值查找对象
     *
     * @param id id值
     * @return Book对象
     */
    Book queryById(Integer id);

    /**
     * 查询书籍的总数量
     * @return  int 书籍总数量
     */
    int count();

    /**
     * 根据分数和时间筛选书籍
     * @param startScore    开始分数
     * @param endScore      结束分数
     * @param startTime     开始时间
     * @param endTime       结束时间
     * @return  List 书籍列表
     */
    List<Book> queryByScoreAndTime(@Param("startScore") Integer startScore, @Param("endScore") Integer endScore,
                                   @Param("startTime") Date startTime, @Param("endTime") Date endTime);


    /**
     * 添加书籍 (在book表中添加)
     * @param itemid id
     * @param author 作者
     * @param publisher 出版社
     * @return 1为成功，0为失败
     */
    int addBook(@Param("itemid") String itemid,@Param("author") String author, @Param("publisher") String publisher);

    /**
     * 按照书名查询
     * @param bookname 书名
     * @return 书籍
     */
    Book queryByNameBook(@Param("bookname") String bookname);

    /**
     * 按照书id查询
     * @param bookid 书籍id
     * @return 书籍
     */
    Book queryByIdBook(@Param("bookid") String bookid);

    /**
     * 删除书籍
     * @param bookid 书籍id
     * @return 1为成功，0为失败
     */
    int deleteBook(@Param("bookid") String bookid);

    /**
     * 更新书籍信息
     * @param bookname 书名
     * @param author 作者
     * @param publisher 出版社
     * @param releaseDate 出版时间
     * @param score 评分
     * @param description 描述
     * @param mainPic 海报
     * @param describePic 详细海报
     * @return 1为成功，0为失败
     */
    int updateBook(@Param("bookid")String bookid,@Param("bookname") String bookname, @Param("author") String author, @Param("publisher") String publisher, @Param("releaseDate") Date releaseDate, @Param("score") double score, @Param("description") String description, @Param("mainPic") String mainPic,@Param("describePic") String describePic);

    /**
     * 查询所有书籍
     * @return List
     */
    List<Book> queryAllBook();
}
