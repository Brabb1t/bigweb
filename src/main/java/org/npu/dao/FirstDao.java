package org.npu.dao;

import org.npu.entity.FirstUsers;

import java.util.List;

public interface FirstDao {
    /**
     * 查找所有用户
     * @return  用户列表
     */
    List<FirstUsers> findAll();

    /**
     * 添加用户
     * @param user  需要加入的用户对象
     * @return  1为成功，0为失败
     */
    int addUser(FirstUsers user);
}
