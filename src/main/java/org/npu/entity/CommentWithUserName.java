package org.npu.entity;

import java.util.Date;

public class CommentWithUserName extends Comment{

    private String userName;

    public CommentWithUserName(Integer id, Integer itemId, Integer userId, Date commentTime, String context, Integer score, Integer checked, String userName) {
        super(id, itemId, userId, commentTime, context, score, checked);
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
