package org.npu.entity;

import java.util.Date;

public class Music extends Item{

    Integer id;
    String singer;
    String album;

    public Music(Integer id, String name, String description, double score, Date releaseDate, String mainPic, String describePic, String singer, String album) {
        super(id, name, description, score, releaseDate, mainPic, describePic);
        this.id = id;
        this.singer = singer;
        this.album = album;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    @Override
    public String toString() {
        return "music{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", score=" + score +
                ", releaseDate=" + releaseDate +
                ", mainPic='" + mainPic + '\'' +
                ", describePic='" + describePic + '\'' +
                ", singer='" + singer + '\'' +
                ", album='" + album + '\'' +
                '}';
    }
}
