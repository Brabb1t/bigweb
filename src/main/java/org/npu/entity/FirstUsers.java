package org.npu.entity;

/**
 * 这是测试使用的对象，不是网站中使用的对象！
 */
public class FirstUsers {
    private int Id;
    private String name;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FirstUsers(int id, String name) {
        Id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "FirstUsers{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                '}';
    }
}

