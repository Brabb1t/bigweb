package org.npu.entity;

import java.util.Date;

public class Comment {
    Integer id;
    Integer itemId;
    Integer userId;
    Date commentTime;
    String context;
    Integer score;
    Integer checked;

    public Comment(Integer id, Integer itemId, Integer userId, Date commentTime, String context, Integer score, Integer checked) {
        this.id = id;
        this.itemId = itemId;
        this.userId = userId;
        this.commentTime = commentTime;
        this.context = context;
        this.score = score;
        this.checked = checked;
    }

    public Comment(Integer itemId, Integer userId, Date commentTime, String context, Integer score, Integer checked) {
        this.itemId = itemId;
        this.userId = userId;
        this.commentTime = commentTime;
        this.context = context;
        this.score = score;
        this.checked = checked;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getChecked() {
        return checked;
    }

    public void setChecked(Integer checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", itemId=" + itemId +
                ", userId=" + userId +
                ", commentTime=" + commentTime +
                ", context='" + context + '\'' +
                ", score=" + score +
                ", checked=" + checked +
                '}';
    }
}

