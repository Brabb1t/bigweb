package org.npu.entity;

public class User {

    Integer id;
    String username;
    String password;
    String email;
    String header;
    String description;


    public User(Integer id, String username, String password, String email, String header, String description) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.header = header;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", header='" + header + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
