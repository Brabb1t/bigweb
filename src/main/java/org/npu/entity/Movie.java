package org.npu.entity;

import java.util.Date;

public class Movie extends Item {
    Integer id;
    String director;
    String actor;
    String actress;

    public Movie(Integer id, String name, String description, double score, Date releaseDate, String mainPic, String describePic, String director, String actor, String actress) {
        super(id, name, description, score, releaseDate, mainPic, describePic);
        this.id = id;
        this.director = director;
        this.actor = actor;
        this.actress = actress;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getActress() {
        return actress;
    }

    public void setActress(String actress) {
        this.actress = actress;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", score=" + score +
                ", releaseDate=" + releaseDate +
                ", mainPic='" + mainPic + '\'' +
                ", describePic='" + describePic + '\'' +
                ", director='" + director + '\'' +
                ", actor='" + actor + '\'' +
                ", actress='" + actress + '\'' +
                '}';
    }
}
