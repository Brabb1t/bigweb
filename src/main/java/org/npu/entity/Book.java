package org.npu.entity;

import java.util.Date;

public class Book extends Item {
    Integer id;
    String author;
    String publisher;

    public Book(Integer id, String name, String description, double score, Date releaseDate, String mainPic, String describePic, String author, String publisher) {
        super(id, name, description, score, releaseDate, mainPic, describePic);
        this.id = id;
        this.author = author;
        this.publisher = publisher;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", score=" + score +
                ", releaseDate=" + releaseDate +
                ", mainPic='" + mainPic + '\'' +
                ", describePic='" + describePic + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
