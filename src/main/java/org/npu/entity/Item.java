package org.npu.entity;

import java.util.Date;

public class Item {
    Integer id;
    String name;
    String description;
    double score;
    Date releaseDate;
    String mainPic;
    String describePic;

    public Item() {
    }

    public Item(Integer id, String name, String description, double score, Date releaseDate, String mainPic, String describePic) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.score = score;
        this.releaseDate = releaseDate;
        this.mainPic = mainPic;
        this.describePic = describePic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getMainPic() {
        return mainPic;
    }

    public void setMainPic(String mainPic) {
        this.mainPic = mainPic;
    }

    public String getDescribePic() {
        return describePic;
    }

    public void setDescribePic(String describePic) {
        this.describePic = describePic;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", score=" + score +
                ", releaseDate=" + releaseDate +
                ", mainPic='" + mainPic + '\'' +
                ", describePic='" + describePic + '\'' +
                '}';
    }
}
