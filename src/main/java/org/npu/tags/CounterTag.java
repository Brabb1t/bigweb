package org.npu.tags;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.jar.JarException;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author : Rehemaiti
 * @Date : 2021-11-29-10:22
 * @Description :
 */
public class CounterTag extends SimpleTagSupport {

    // 计数器
    private static int counter = 1;

    @Override
    public void doTag() throws javax.servlet.jsp.SkipPageException, IOException {
        try {
            JspWriter out = getJspContext().getOut();
            out.print(counter);
            setCounter();
        } catch (JarException e) {
            throw new javax.servlet.jsp.SkipPageException(e.getMessage());
        }
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter() {
        counter++;
    }
}
