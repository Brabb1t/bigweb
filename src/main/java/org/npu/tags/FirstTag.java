package org.npu.tags;

import org.npu.entity.Movie;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.ws.spi.http.HttpContext;

/**
 * 实现 mytag:test
 */
public class FirstTag extends TagSupport {
    public int doStartTag() throws JspTagException {
        ServletContext application = pageContext.getServletContext();
        try {
            JspWriter out = pageContext.getOut();
            Movie oneMovie = (Movie) application.getAttribute("oneMovie");
            String movieName = oneMovie.getName();
            String outPrint = "TagTest" + movieName;
            out.print(outPrint);
        } catch (java.io.IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
