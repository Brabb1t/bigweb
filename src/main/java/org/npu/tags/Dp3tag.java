package org.npu.tags;

import org.npu.entity.Movie;
import org.npu.entity.Music;
import org.npu.entity.Book;


import javax.servlet.ServletContext;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.ws.spi.http.HttpContext;

public class Dp3tag extends TagSupport {

    public int doStartTag() throws JspTagException {
        ServletContext application = pageContext.getServletContext();
        try {
            JspWriter out = pageContext.getOut();
            Movie oneMovie = (Movie) application.getAttribute("oneMovie");
            Music oneMusic=(Music)application.getAttribute("oneMusic");
            Book oneBook=(Book)application.getAttribute("oneBook");
            String movieName = oneMovie.getName();
            String musicName = oneMusic.getName();
            String bookName = oneBook.getName();
            String moviePic=oneMovie.getMainPic();
            String musicPic=oneMusic.getMainPic();
            String bookPic=oneBook.getMainPic();
            String outPrint = "TagTest" + movieName;
            out.print("   <div class=\"displaythree\" style=\"width: 1500px;height: 700px;margin-top: 40px;\">\n" +
                    "       <div style=\"height: 600px;width: 350px;margin-left: 50px;float: left;text-align: center;\">\n" +
                    "           <a href=\"/bigweb/detail?id="+oneMovie.getId()+"&cPage=1&cPageSize=10\">\n" +
                    "               <img src=\""+moviePic+"\" style=\"width: 100%;height: 90%\">\n" +
                    "               <i style=\"text-decoration: none\">"+movieName+"</i>\n" +
                    "           </a>\n" +
                    "       </div>\n" +
                    "       <div style=\"height: 600px;width: 350px;margin-left: 50px;float: left;text-align: center;\">\n" +
                    "           <a href=\"/bigweb/detail?id="+oneMusic.getId()+"&cPage=1&cPageSize=10\">\n" +
                    "               <img src=\""+musicPic+"\" style=\"width: 100%;height: 90%\">\n" +
                    "               <i>"+musicName+"</i>\n" +
                    "           </a>\n" +
                    "       </div>\n" +
                    "       <div style=\"height: 600px;width: 350px;margin-left: 50px;float: left;text-align: center;\">\n" +
                    "           <a href=\"/bigweb/detail?id="+oneBook.getId()+"&cPage=1&cPageSize=10\">\n" +
                    "               <img src=\""+bookPic+"\" style=\"width: 100%;height: 90%\">\n" +
                    "               <i>"+bookName+"</i>\n" +
                    "           </a>\n" +
                    "       </div>\n" +
                    "\n" +
                    "   </div>");

        } catch (java.io.IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
