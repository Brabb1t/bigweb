package org.npu.controller;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.MusicDao;
import org.npu.entity.Music;
import org.npu.util.DateUtil;
import org.npu.util.MyBatisUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class MusicServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //请求展示前十个音乐。路径应该为/bigweb/music?page=1&pageSize=10
        int page = Integer.parseInt(req.getParameter("page"));//展示第n页
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));//一页展示多少

        int startIndex = (page - 1) * 10;//数据库检索的开始下标

        //获取数据库操作对象
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        MusicDao musicDao = sqlSession.getMapper(MusicDao.class);

        //将所有音乐和音乐的数量获取到并交给前端
        List<Music> musicList = musicDao.queryList(startIndex, pageSize);
        int musicAmount = musicDao.count();
        int totalPage = (musicAmount + pageSize - 1) / pageSize;
        //musicList.forEach(music -> System.out.println(music));

        req.setAttribute("musicList", musicList);
        req.setAttribute("page", page);
        req.setAttribute("totalPage", totalPage);

        req.getRequestDispatcher("/musicpage").forward(req, resp);//musicpage展示页待完成
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //当用户根据年份，好评度进行搜索时。向servlet发送post请求
        //路径为/bigweb/music
        String score = req.getParameter("score");//应该有4种情况null,good,normal,bad
        String time = req.getParameter("year");//3种情况：null,tmp,before

        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        MusicDao musicDao = sqlSession.getMapper(MusicDao.class);

        //根据分数分类
        Integer startScore, endScore;
        if (score.equals("good")){
            startScore = 8;
            endScore = 10;
        }else if (score.equals("normal")){
            startScore = 5;
            endScore = 7;
        }else if (score.equals("bad")){
            startScore = 0;
            endScore = 4;
        }else {
            startScore = null;
            endScore = null;
        }

        //根据时间分类
        Date startTime, endTime;
        DateUtil dateUtil = new DateUtil();
        if (time.equals("tmp")){
            startTime = dateUtil.getCurrentYearStartTime();
            endTime = dateUtil.getCurrentYearEndTime();
        }else if (time.equals("before")){
            endTime = dateUtil.getCurrentYearStartTime();
            startTime = dateUtil.getEarlyTime();
        }else {
            startTime = null;
            endTime = null;
        }

        List<Music> musicList = musicDao.queryByScoreAndTime(startScore, endScore, startTime, endTime);
        Integer page = 1;
        Integer totalPage = 1;

        req.setAttribute("musicList", musicList);
        req.setAttribute("page", page);
        req.setAttribute("totalPage", totalPage);
        req.getRequestDispatcher("/musicpage").forward(req, resp);
    }
}
