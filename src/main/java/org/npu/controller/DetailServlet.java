package org.npu.controller;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.*;
import org.npu.entity.*;
import org.npu.util.MyBatisUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DetailServlet extends HttpServlet {
    //详情页，用来展示详细介绍。访问地址应该是/bigweb/detail?id=1&cPage=1&cPageSize=10
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Integer id = Integer.valueOf(req.getParameter("id"));
        Integer cPage = Integer.parseInt(req.getParameter("cPage"));
        Integer cPageSize = Integer.parseInt(req.getParameter("cPageSize"));

        //数据库数据下标位置
        int startIndex = (cPage - 1) * 10;//数据库检索的开始下标

        //获取数据库操作对象
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BookDao bookDao = sqlSession.getMapper(BookDao.class);
        MovieDao movieDao = sqlSession.getMapper(MovieDao.class);
        MusicDao musicDao = sqlSession.getMapper(MusicDao.class);
        UserDao userDao = sqlSession.getMapper(UserDao.class);
        CommentDao commentDao = sqlSession.getMapper(CommentDao.class);

        //这其中有两个为空，一个有内容
        Movie oneMovie = movieDao.queryById(id);
        Music oneMusic = musicDao.queryById(id);
        Book oneBook = bookDao.queryById(id);
        List<Comment> commentList = commentDao.queryByIdWithLimit(id, startIndex, cPageSize);
        List<User> userList = userDao.getList();
        List<CommentWithUserName> cuList = new ArrayList();//最后给前端的带有用户姓名的评论列表

        //往cuList里添加数据
        for (int i = 0; i < commentList.size(); i++) {
            Comment index = commentList.get(i);
            String userName = "";
            for (int j = 0; j < userList.size(); j++) {
                if (index.getUserId().equals(userList.get(j).getId())) {
                    userName = userList.get(j).getUsername();
                    break;
                }
            }
            CommentWithUserName tmpComment = new CommentWithUserName(index.getId(), index.getItemId(), index.getUserId(),
                    index.getCommentTime(), index.getContext(), index.getScore(), index.getChecked(), userName);
            cuList.add(tmpComment);
        }

        //设置评论信息
        Integer commentAmount = commentDao.count(id);
        Integer cTotalPage = (commentAmount + cPageSize - 1) / cPageSize;

        req.setAttribute("id", id);
        req.setAttribute("chosenMovie", oneMovie);
        req.setAttribute("chosenMusic", oneMusic);
        req.setAttribute("chosenBook", oneBook);
        req.setAttribute("cuList", cuList);//需要展示的评论列表
        req.setAttribute("cPage",cPage);//当前评论是第几页
        req.setAttribute("cTotalPage", cTotalPage);//评论总共有几页


//        System.out.println("mo==" + oneMovie);
//        System.out.println("mu==" + oneMusic);
//        System.out.println("bo==" + oneBook);
//        commentList.forEach(c -> System.out.println(c));

        req.getRequestDispatcher("/detailpage").forward(req, resp);

    }
}
