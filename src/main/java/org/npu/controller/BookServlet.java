package org.npu.controller;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.BookDao;
import org.npu.entity.Book;
import org.npu.util.DateUtil;
import org.npu.util.MyBatisUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class BookServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //请求展示前十个书籍。路径应该为/bigweb/book?page=1&pageSize=10
        int page = Integer.parseInt(req.getParameter("page"));//展示第n页
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));//一页展示多少

        int startIndex = (page - 1) * 10;//数据库检索的开始下标

        //获取数据库操作对象
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BookDao bookDao = sqlSession.getMapper(BookDao.class);

        //将所有书籍和数量获取到并交给前端
        List<Book> bookList = bookDao.queryList(startIndex, pageSize);
        int bookAmount = bookDao.count();
        int totalPage = (bookAmount + pageSize - 1) / pageSize;

        req.setAttribute("bookList", bookList);
        req.setAttribute("page", page);
        req.setAttribute("totalPage", totalPage);
        //bookList.forEach(book -> System.out.println(book));

        req.getRequestDispatcher("/bookpage").forward(req, resp);//bookpage展示页待完成

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //当用户根据年份，好评度进行搜索时。向servlet发送post请求
        //路径为/bigweb/book
        String score = req.getParameter("score");//应该有4种情况null,good,normal,bad
        String time = req.getParameter("year");//3种情况：null,tmp,before

        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BookDao bookDao = sqlSession.getMapper(BookDao.class);

        //根据分数分类
        Integer startScore, endScore;
        if (score.equals("good")){
            startScore = 8;
            endScore = 10;
        }else if (score.equals("normal")){
            startScore = 5;
            endScore = 7;
        }else if (score.equals("bad")){
            startScore = 0;
            endScore = 4;
        }else {
            startScore = null;
            endScore = null;
        }

        //根据时间分类
        Date startTime, endTime;
        DateUtil dateUtil = new DateUtil();
        if (time.equals("tmp")){
            startTime = dateUtil.getCurrentYearStartTime();
            endTime = dateUtil.getCurrentYearEndTime();
        }else if (time.equals("before")){
            endTime = dateUtil.getCurrentYearStartTime();
            startTime = dateUtil.getEarlyTime();
        }else {
            startTime = null;
            endTime = null;
        }

        List<Book> bookList = bookDao.queryByScoreAndTime(startScore, endScore, startTime, endTime);
        Integer page = 1;
        Integer totalPage = 1;

        req.setAttribute("bookList", bookList);
        req.setAttribute("page", page);
        req.setAttribute("totalPage", totalPage);
        req.getRequestDispatcher("/bookpage").forward(req, resp);
    }
}
