package org.npu.controller;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.FirstDao;
import org.npu.entity.FirstUsers;
import org.npu.util.MyBatisUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

// 测试访问地址：http://localhost:8080/bigweb/test
public class FirstServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");
        PrintWriter out = resp.getWriter();
        out.println("测试");

        /**
         * 使用mybatis的动态代理机制，是同SqlSession.getMapper(dao接口)
         * getMapper能获取dao接口对于的实现类对象。
         */
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        FirstDao dao = sqlSession.getMapper(FirstDao.class);
        //调用dao的方法，执行数据库的操作
        List<FirstUsers> data = dao.findAll();
        out.println(data);

        //test

        //test2

        out.close();
    }
}
