package org.npu.controller;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.UserDao;
import org.npu.util.MyBatisUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String name = req.getParameter("username");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        //获取操作数据库的对象
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UserDao dao = sqlSession.getMapper(UserDao.class);

        int result = dao.Register(name, email, password);
        //添加完成后提交事务
        sqlSession.commit();
        if (result == 1) {
            //注册成功
            resp.sendRedirect("/bigweb/loginpage");
        } else {
            resp.sendRedirect("/bigweb/registerpage");
        }
    }
}
