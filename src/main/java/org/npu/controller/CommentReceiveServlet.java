package org.npu.controller;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.CommentDao;
import org.npu.dao.UserDao;
import org.npu.entity.Comment;
import org.npu.entity.User;
import org.npu.util.MyBatisUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class CommentReceiveServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //进来的用户是已登录的用户，因为filter

        //添加留言
        req.setCharacterEncoding("utf-8");

        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UserDao userDao = sqlSession.getMapper(UserDao.class);
        CommentDao commentDao = sqlSession.getMapper(CommentDao.class);

        String username = "";
        String password = "";
        Integer itemId = Integer.valueOf(req.getParameter("itemId"));
        String context = req.getParameter("context");
        Integer score = Integer.parseInt(req.getParameter("score"));

        //从cookie中拿用户名和密码
        Cookie[] cookieArray = null;
        cookieArray = req.getCookies();//读取Cookie
        for (Cookie c : cookieArray) {
            String name = c.getName();
            String value = c.getValue();
            if (name.equals("username")) username = value;
            else if (name.equals("password")) password = value;
        }

        Integer userId = userDao.getId(username, password);//拿到用户的id
        Date date = new Date();
        Comment comment = new Comment(itemId, userId, date, context, score, 1);//默认通过该条评论

        int result = commentDao.add(comment);
        sqlSession.commit();//提交评论

        /*if (result == 1) {
            System.out.println("评论添加成功");
        } else {
            System.out.println("评论添加失败");
        }*/
        //System.out.println(context);
        resp.sendRedirect("/bigweb/detail?id=" + itemId + "&cPage=1&cPageSize=10");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //删除留言操作（即将留言的checked改为0）
        Integer commentId = Integer.valueOf(req.getParameter("commentId"));
        String res = req.getParameter("res");//请求发送来源

        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        CommentDao commentDao = sqlSession.getMapper(CommentDao.class);

        int result = commentDao.delete(commentId);
        sqlSession.commit();
        if (result == 1) {
            System.out.println("评论删除成功");
        } else {
            System.out.println("评论删除失败");
        }

        if (res.equals("user")) {
            req.getRequestDispatcher("/meschange").forward(req, resp);
        } else if (res.equals("admin")) {
            req.getRequestDispatcher("/adminpage").forward(req, resp);
        }

    }
}
