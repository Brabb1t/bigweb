package org.npu.controller;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.*;
import org.npu.entity.*;
import org.npu.util.MyBatisUtils;
import org.npu.util.UserMesChange;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static java.lang.Double.parseDouble;

public class AdminOperationServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        /*网页编码格式配置*/
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html; charset=utf-8");

        /*sql会话配置*/
        SqlSession sqlSession = MyBatisUtils.getSqlSession();

        /*管理员操作内容*/
        String operation = (String) req.getParameter("operation");

        /*若没有选择具体操作，弹窗报错并回到管理员主页面*/
        if (operation == null) {
            System.out.println("操作为空");
            resp.getWriter().println("<!DOCTYPE html>" +
                    "<html lang='zh'>" +
                    "<head>" +
                    "<meta charset=UTF-8>" +
                    "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                    "</head>" +
                    "<body>" +
                    "<script>alert('操作为空,请选择操作');</script>" +
                    "</body>" +
                    "</html>"
            );

//////////////////////////////////////////user//////////////////////////////////////////////////////////////////////////

            /*添加用户*/
        } else if (operation.equals("addUser")) {

            String name = req.getParameter("username");
            String email = req.getParameter("email");
            String password = req.getParameter("password");

            UserMesChange userUtil = new UserMesChange();
            int result = userUtil.addUser(name, email, password);

            // 添加成功
            if (result == 1) {
                System.out.println("添加用户成功");

                // 返回全部用户信息以便查看编辑后的用户信息
                UserDao userDao = sqlSession.getMapper(UserDao.class);
                List<User> userList = userDao.getList();
                req.setAttribute("userList", userList);

                // 添加失败
            } else {
                System.out.println("添加用户失败！");

                // 返回全部用户信息以便查看编辑后的用户信息
                UserDao userDao = sqlSession.getMapper(UserDao.class);
                List<User> userList = userDao.getList();
                req.setAttribute("userList", userList);

            }

            // 数据反馈给管理员页面
            req.getRequestDispatcher("/adminpage").forward(req, resp);

            /*删除用户信息(按照Id)*/
        } else if (operation.equals("deleteUser")) {

            String id = req.getParameter("id");

            // 获取操作数据库的对象
            UserDao dao = sqlSession.getMapper(UserDao.class);

            int result = dao.deleteUser(id);

            // 添加完成后提交事务
            sqlSession.commit();

            // 删除用户成功
            if (result == 1) {
                System.out.println("删除成功");

                // 删除用户失败
            } else {
                System.out.println("删除失败");

            }

            // 返回全部用户信息以便查看编辑后的用户信息
            UserDao userDao = sqlSession.getMapper(UserDao.class);
            List<User> userList = userDao.getList();
            req.setAttribute("userList", userList);

            // 数据反馈给管理员页面
            req.getRequestDispatcher("/adminpage").forward(req, resp);

            /*查询(根据id)*/
        } else if (operation.equals("queryByIdUser")) {

            String id = req.getParameter("id");

            // 获取操作数据库的对象
            UserDao userdao = sqlSession.getMapper(UserDao.class);

            // 从数据库获取用户相关信息
            User user = userdao.queryById(id);

            // 查询的用户为空
            if (user == null) {
                System.out.println("该用户不存在");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('该用户不存在');</script>" +
                        "</body>" +
                        "</html>"
                );

                // 查询成功
            } else {
                // 向前端传一个用户对象
                req.setAttribute("user", user);

                // 用户所有评论信息
                CommentDao commentDao = sqlSession.getMapper(CommentDao.class);
                List<Comment> commentList = commentDao.queryById(user.getId());
                req.setAttribute("commentList", commentList);

                // 数据反馈给管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);
            }

            /*根据用户名查询*/
        } else if (operation.equals("queryByNameUser")) {
            // 根据用户名邮箱查询用户
            String username = req.getParameter("username");

            // 获取操作数据库的对象
            UserDao userdao = sqlSession.getMapper(UserDao.class);

            // 从数据库获取用户相关信息
            User user = userdao.queryByName(username);

            // 查询的用户为空
            if (user == null) {
                System.out.println("该用户不存在");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('该用户不存在');</script>" +
                        "</body>" +
                        "</html>"
                );

                // 查询成功
            } else {
                // 向前端传一个用户对象
                req.setAttribute("user", user);

                // 用户所有评论信息
                CommentDao commentDao = sqlSession.getMapper(CommentDao.class);
                List<Comment> commentList = commentDao.queryById(user.getId());
                req.setAttribute("commentList", commentList);

                // 数据反馈给管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);
            }

            /*根据id更新用户信息*/
        } else if (operation.equals("updateUser")) {
            // 更新用户信息
            String id = req.getParameter("id");
            String username = req.getParameter("username");
            String email = req.getParameter("email");
            String password = req.getParameter("password");
            String header = req.getParameter("header");
            String description = req.getParameter("description");

            // 获取操作数据库的对象
            UserDao dao = sqlSession.getMapper(UserDao.class);


            // 更新用户信息
            int result = dao.updateUser(id, username, email, password, header, description);

            //添加完成后提交事务
            sqlSession.commit();

            // 更新成功
            if (result == 1) {
                System.out.println("更新成功");

                // 返回全部用户信息以便查看编辑后的用户信息
                UserDao userDao = sqlSession.getMapper(UserDao.class);
                List<User> userList = userDao.getList();
                req.setAttribute("userList", userList);

                // 更新失败
            } else {
                System.out.println("更新失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('更新失败');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            // 数据反馈给管理员页面
            req.getRequestDispatcher("/adminpage").forward(req, resp);

            /*查看所有用户*/
        } else if (operation.equals("queryAllUser")) {

            // 无论如何每次都取所有用户最新信息并传到管理员页面进行展示
            UserDao userDao = sqlSession.getMapper(UserDao.class);
            List<User> userList = userDao.getList();
            req.setAttribute("userList", userList);
            req.getRequestDispatcher("/adminpage").forward(req, resp);

//////////////////////////////////////////////book//////////////////////////////////////////////////////////////////////

            /*添加书籍*/
        } else if (operation.equals("addBook")) {

            String bookname = req.getParameter("bookname");
            String author = req.getParameter("author");
            String publisher = req.getParameter("publisher");

            // 出版日期转换
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date releaseDate = new Date();
            try {
                releaseDate = sdf.parse(req.getParameter("releaseDate"));
            } catch (ParseException e) {
                // e.printStackTrace();
            }

            double score = parseDouble(req.getParameter("score"));
            String description = req.getParameter("description");
            String mainPic = req.getParameter("mainPic");
            String describePic = req.getParameter("describePic");

            // 书籍添加到item表中
            ItemDao itemDao = sqlSession.getMapper(ItemDao.class);
            int result1 = itemDao.addItem(bookname, releaseDate, score, description, mainPic, describePic);

            // 书籍添加到book表中
            BookDao bookDao = sqlSession.getMapper(BookDao.class);
            String itemid = itemDao.getLastItem().getId().toString();
            int result2 = bookDao.addBook(itemid, author, publisher);

            //添加完成后提交事务
            sqlSession.commit();

            // 书籍添加成功
            if (result1 == 1 && result2 == 1) {
                System.out.println("书籍添加成功");

                // 获取全部书籍信息,以便管理员查看
                List<Book> bookList = bookDao.queryAllBook();
                req.setAttribute("bookList", bookList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 书籍添加失败
            } else {
                System.out.println("书籍添加失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('添加失败，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*查询书籍（按照书籍id）*/
        } else if (operation.equals("queryByIdBook")) {

            String bookid = req.getParameter("bookid");

            BookDao bookDao = sqlSession.getMapper(BookDao.class);
            Book book = bookDao.queryByIdBook(bookid);

            // 查询失败
            if (book == null) {
                System.out.println("该书籍不存在");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('查询错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );

                // 查询成功
            } else {
                // 查询的书籍数据传到管理员页面进行展示
                req.setAttribute("book", book);

                // 返回这本书相关评论到主页面
                CommentDao commentDao = sqlSession.getMapper(CommentDao.class);
                List<Comment> commentList = commentDao.queryByItemId(book.getId());

                // 查询该书籍相关的评论传到管理员页面进行展示
                req.setAttribute("bookcommentList", commentList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);
            }

            /*查询书记（按照书名）*/
        } else if (operation.equals("queryByNameBook")) {
            String bookname = req.getParameter("bookname");

            BookDao bookDao = sqlSession.getMapper(BookDao.class);
            Book book = bookDao.queryByNameBook(bookname);

            // 查询失败
            if (book == null) {
                System.out.println("该书籍不存在");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('查询错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );

                // 查询成功
            } else {
                // 查询的书籍数据传到管理员页面进行展示
                req.setAttribute("book", book);

                // 返回这本书相关评论到主页面
                CommentDao commentDao = sqlSession.getMapper(CommentDao.class);
                List<Comment> commentList = commentDao.queryByItemId(book.getId());

                // 查询该书籍相关的评论传到管理员页面进行展示
                req.setAttribute("bookcommentList", commentList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);
            }

            /*删除书籍（按照书籍id）*/
        } else if (operation.equals("deleteBook")) {

            String bookid = req.getParameter("bookid");

            // 从book中删除
            BookDao bookDao = sqlSession.getMapper(BookDao.class);
            int result1 = bookDao.deleteBook(bookid);
            // 从item中删除
            ItemDao itemDao = sqlSession.getMapper(ItemDao.class);
            String itemid = bookid;
            int result2 = itemDao.deleteById(itemid);

            // 添加完成后提交事务
            sqlSession.commit();

            // 删除成功
            if (result1 == 1 && result2 == 1) {
                System.out.println("删除成功");

                // 获取全部书籍信息,以便管理员查看
                List<Book> bookList = bookDao.queryAllBook();
                req.setAttribute("bookList", bookList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 删除失败
            } else {
                System.out.println("删除失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('删除错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*更新书籍信息（按照书籍id）*/
        } else if (operation.equals("updateBook")) {

            String bookid = req.getParameter("bookid");
            String bookname = req.getParameter("bookname");
            String author = req.getParameter("author");
            String publisher = req.getParameter("publisher");

            // 出版日期转换
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date releaseDate = new Date();
            try {
                releaseDate = sdf.parse(req.getParameter("releaseDate"));
            } catch (ParseException e) {
                // e.printStackTrace();
            }

            double score = parseDouble(req.getParameter("score"));
            String description = req.getParameter("description");
            String mainPic = req.getParameter("mainPic");
            String describePic = req.getParameter("describePic");

            BookDao bookDao = sqlSession.getMapper(BookDao.class);
            int result = bookDao.updateBook(bookid, bookname, author, publisher, releaseDate, score, description, mainPic, describePic);

            // 添加完成后提交事务
            sqlSession.commit();

            // 更新成功
            if (result != 1) {
                System.out.println("更新成功");

                // 获取全部书籍信息,以便管理员查看
                List<Book> bookList = bookDao.queryAllBook();
                req.setAttribute("bookList", bookList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 更新失败
            } else {
                System.out.println("更新失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('更新失败，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*查看全部书籍*/
        } else if (operation.equals("queryAllBook")) {

            // 获取全部书籍信息,以便管理员查看
            BookDao bookDao = sqlSession.getMapper(BookDao.class);
            List<Book> bookList = bookDao.queryAllBook();
            req.setAttribute("bookList", bookList);
            req.getRequestDispatcher("/adminpage").forward(req, resp);

///////////////////////////////////////////////////////movie////////////////////////////////////////////////////////////

            /*添加电影*/
        } else if (operation.equals("addMovie")) {

            String moviename = req.getParameter("moviename");
            String director = req.getParameter("director");
            String actor = req.getParameter("actor");
            String actress = req.getParameter("actress");

            // 出版日期转换
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date releaseDate = new Date();
            try {
                releaseDate = sdf.parse(req.getParameter("releaseDate"));
            } catch (ParseException e) {
                // e.printStackTrace();
            }

            double score = parseDouble(req.getParameter("score"));
            String description = req.getParameter("description");
            String mainPic = req.getParameter("mainPic");
            String describePic = req.getParameter("describePic");

            // 电影添加到item表中
            ItemDao itemDao = sqlSession.getMapper(ItemDao.class);
            int result = itemDao.addItem(moviename, releaseDate, score, description, mainPic, describePic);

            // 电影添加到movie表中
            MovieDao movieDao = sqlSession.getMapper(MovieDao.class);
            String itemid = itemDao.getLastItem().getId().toString();
            movieDao.addMovie(itemid, director, actor, actress);

            // 添加完成后提交事务
            sqlSession.commit();

            // 电影添加成功
            if (result == 1) {
                System.out.println("电影添加成功");

                // 获取全部电影信息,以便管理员查看
                List<Movie> movieList = movieDao.queryAllMovie();
                req.setAttribute("movieList", movieList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 电影添加失败
            } else {
                System.out.println("电影添加失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('添加失败，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*查询电影(根据id)*/
        } else if (operation.equals("queryByIdMovie")) {

            String movieid = req.getParameter("movieid");

            MovieDao movieDao = sqlSession.getMapper(MovieDao.class);
            Movie movie = movieDao.queryByIdMovie(movieid);

            // 查询失败
            if (movie == null) {
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('查询错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );

                // 查询成功
            } else {

                // 查询的电影数据传到管理员页面进行展示
                req.setAttribute("movie", movie);

                // 返回这本电影相关评论到主页面
                CommentDao commentDao = sqlSession.getMapper(CommentDao.class);
                List<Comment> commentList = commentDao.queryById(movie.getId());
                req.setAttribute("moviecommentList", commentList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);
            }

            /*查询电影(根据电影名)*/
        } else if (operation.equals("queryByNameMovie")) {

            String moviename = req.getParameter("moviename");

            MovieDao movieDao = sqlSession.getMapper(MovieDao.class);
            Movie movie = movieDao.queryByNameMovie(moviename);

            // 查询失败
            if (movie == null) {
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('查询错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );

                // 查询成功
            } else {

                // 查询的电影数据传到管理员页面进行展示
                req.setAttribute("movie", movie);

                // 返回这部电影相关评论到主页面
                CommentDao commentDao = sqlSession.getMapper(CommentDao.class);
                List<Comment> commentList = commentDao.queryById(movie.getId());
                req.setAttribute("moviecommentList", commentList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);
            }

            /*删除电影(根据id)*/
        } else if (operation.equals("deleteMovie")) {

            String movieid = req.getParameter("movieid");

            // 从movie中删除
            MovieDao movieDao = sqlSession.getMapper(MovieDao.class);
            int result1 = movieDao.deleteMovie(movieid);

            // 从item中删除
            ItemDao itemDao = sqlSession.getMapper(ItemDao.class);
            String itemid = movieid;
            int result2 = itemDao.deleteById(itemid);

            // 添加完成后提交事务
            sqlSession.commit();

            // 删除成功
            if (result1 == 1 && result2 == 1) {
                System.out.println("删除成功");

                // 获取全部电影信息,以便管理员查看
                List<Movie> movieList = movieDao.queryAllMovie();
                req.setAttribute("movieList", movieList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 删除失败
            } else {
                System.out.println("删除失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('删除错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*更新电影(根据id)*/
        } else if (operation.equals("updateMovie")) {

            String movieid = req.getParameter("movieid");
            String moviename = req.getParameter("moviename");
            String director = req.getParameter("director");
            String actor = req.getParameter("actor");
            String actress = req.getParameter("actress");

            // 出版日期转换
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date releaseDate = new Date();
            try {
                releaseDate = sdf.parse(req.getParameter("releaseDate"));
            } catch (ParseException e) {
                // e.printStackTrace();
            }

            double score = parseDouble(req.getParameter("score"));
            String description = req.getParameter("description");
            String mainPic = req.getParameter("mainPic");
            String describePic = req.getParameter("describePic");

            MovieDao movieDao = sqlSession.getMapper(MovieDao.class);
            int result = movieDao.updateMovie(movieid, moviename, director, actor, actress, releaseDate, score, description, mainPic, describePic);

            // 添加完成后提交事务
            sqlSession.commit();

            // 更新成功
            if (result != 1) {
                System.out.println("更新成功");

                // 获取全部电影信息,以便管理员查看
                List<Movie> movieList = movieDao.queryAllMovie();
                req.setAttribute("movieList", movieList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 更新失败
            } else {
                System.out.println("更新失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('更新失败，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*查询所有电影*/
        } else if (operation.equals("queryAllMovie")) {

            // 获取全部电影信息,以便管理员查看
            MovieDao movieDao = sqlSession.getMapper(MovieDao.class);
            List<Movie> movieList = movieDao.queryAllMovie();
            req.setAttribute("movieList", movieList);
            req.getRequestDispatcher("/adminpage").forward(req, resp);

///////////////////////////////////////////////////////music////////////////////////////////////////////////////////////

            /*添加音乐*/
        } else if (operation.equals("addMusic")) {

            String musicname = req.getParameter("musicname");
            String singer = req.getParameter("singer");
            String album = req.getParameter("album");

            // 出版日期转换
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date releaseDate = new Date();
            try {
                releaseDate = sdf.parse(req.getParameter("releaseDate"));
            } catch (ParseException e) {
                // e.printStackTrace();
            }

            double score = parseDouble(req.getParameter("score"));
            String description = req.getParameter("description");
            String mainPic = req.getParameter("mainPic");
            String describePic = req.getParameter("describePic");

            // 音乐添加到item表中
            ItemDao itemDao = sqlSession.getMapper(ItemDao.class);
            int result = itemDao.addItem(musicname, releaseDate, score, description, mainPic, describePic);

            // 音乐添加到music表中
            MusicDao musicDao = sqlSession.getMapper(MusicDao.class);
            String itemid = itemDao.getLastItem().getId().toString();
            musicDao.addMusic(itemid, singer, album);

            // 添加完成后提交事务
            sqlSession.commit();

            // 音乐添加成功
            if (result == 1) {
                System.out.println("音乐添加成功");

                // 获取全部音乐信息,以便管理员查看
                List<Music> musicList = musicDao.queryAllMusic();
                req.setAttribute("musicList", musicList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 音乐添加失败
            } else {
                System.out.println("音乐添加失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('添加失败，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*查询音乐(根据id)*/
        } else if (operation.equals("queryByIdMusic")) {

            String musicid = req.getParameter("musicid");

            MusicDao musicDao = sqlSession.getMapper(MusicDao.class);
            Music music = musicDao.queryByIdMusic(musicid);

            // 查询失败
            if (music == null) {
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('查询错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );

                // 查询成功
            } else {
                // 查询的音乐数据传到管理员页面进行展示
                req.setAttribute("music", music);

                // 返回这首音乐相关评论到主页面
                CommentDao commentDao = sqlSession.getMapper(CommentDao.class);
                List<Comment> commentList = commentDao.queryById(music.getId());
                System.out.println("musiccommentList:" + commentList);

                req.setAttribute("musiccommentList", commentList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);
            }

            /*查询音乐(根据音乐名)*/
        } else if (operation.equals("queryByNameMusic")) {

            String musicname = req.getParameter("musicname");

            MusicDao musicDao = sqlSession.getMapper(MusicDao.class);
            Music music = musicDao.queryByNameMusic(musicname);

            // 查询失败
            if (music == null) {
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('查询错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );

                // 查询成功
            } else {
                // 查询的音乐数据传到管理员页面进行展示
                req.setAttribute("music", music);

                // 返回这本书相关评论到主页面
                CommentDao commentDao = sqlSession.getMapper(CommentDao.class);
                List<Comment> commentList = commentDao.queryById(music.getId());
                System.out.println(commentList);

                req.setAttribute("musiccommentList", commentList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);
            }

            /*删除音乐(根据id)*/
        } else if (operation.equals("deleteMusic")) {

            String musicid = req.getParameter("musicid");

            // 从movie中删除
            MusicDao musicDao = sqlSession.getMapper(MusicDao.class);
            int result1 = musicDao.deleteMusic(musicid);

            // 从item中删除
            ItemDao itemDao = sqlSession.getMapper(ItemDao.class);
            String itemid = musicid;
            int result2 = itemDao.deleteById(itemid);

            // 添加完成后提交事务
            sqlSession.commit();

            // 删除成功
            if (result1 == 1 && result2 == 1) {
                System.out.println("删除成功");

                // 获取全部音乐信息,以便管理员查看
                List<Music> musicList = musicDao.queryAllMusic();
                req.setAttribute("musicList", musicList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 删除失败
            } else {
                System.out.println("删除失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('删除错误，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*更新音乐(根据id)*/
        } else if (operation.equals("updateMusic")) {

            String musicid = req.getParameter("musicid");
            String musicname = req.getParameter("musicname");
            String singer = req.getParameter("singer");
            String album = req.getParameter("album");

            // 出版日期转换
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date releaseDate = new Date();
            try {
                releaseDate = sdf.parse(req.getParameter("releaseDate"));
            } catch (ParseException e) {
                // e.printStackTrace();
            }

            double score = parseDouble(req.getParameter("score"));
            String description = req.getParameter("description");
            String mainPic = req.getParameter("mainPic");
            String describePic = req.getParameter("describePic");

            MusicDao musicDao = sqlSession.getMapper(MusicDao.class);
            int result = musicDao.updateMusic(musicid, musicname, singer, album, releaseDate, score, description, mainPic, describePic);

            // 添加完成后提交事务
            sqlSession.commit();

            // 更新成功
            if (result != 1) {
                System.out.println("更新成功");

                // 获取全部音乐信息,以便管理员查看
                List<Music> musicList = musicDao.queryAllMusic();
                req.setAttribute("musicList", musicList);

                // 回到管理员页面
                req.getRequestDispatcher("/adminpage").forward(req, resp);

                // 更新失败
            } else {
                System.out.println("更新失败");
                resp.getWriter().println("<!DOCTYPE html>" +
                        "<html lang='zh'>" +
                        "<head>" +
                        "<meta charset=UTF-8>" +
                        "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                        "</head>" +
                        "<body>" +
                        "<script>alert('更新失败，请重试');</script>" +
                        "</body>" +
                        "</html>"
                );
            }

            /*查询所有音乐*/
        } else if (operation.equals("queryAllMusic")) {

            // 获取全部音乐信息,以便管理员查看
            MusicDao musicDao = sqlSession.getMapper(MusicDao.class);
            List<Music> musicList = musicDao.queryAllMusic();
            req.setAttribute("musicList", musicList);
            req.getRequestDispatcher("/adminpage").forward(req, resp);

////////////////////////////////////////////////////operation error///////////////////////////////////////////////////////////////

            /*以上操作都失败，则会报错并回到主页面*/
        } else {
            System.out.println("操作失败");
            resp.getWriter().println("<!DOCTYPE html>" +
                    "<html lang='zh'>" +
                    "<head>" +
                    "<meta charset=UTF-8>" +
                    "<meta http-equiv='refresh' content='0; url=/bigweb/adminpage'/>" +
                    "</head>" +
                    "<body>" +
                    "<script>alert('操作失败,请重试');</script>" +
                    "</body>" +
                    "</html>"
            );
        }
    }
}
