package org.npu.controller;

import org.apache.ibatis.session.SqlSession;
import org.npu.dao.MovieDao;
import org.npu.entity.Movie;
import org.npu.util.DateUtil;
import org.npu.util.MyBatisUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class MovieServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //请求展示前十个影片。路径应该为/bigweb/movie?page=1&pageSize=10
        int page = Integer.parseInt(req.getParameter("page"));//展示第n页
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));//一页展示多少

        int startIndex = (page - 1) * 10;//数据库检索的开始下标

        //获取数据库操作对象
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        MovieDao movieDao = sqlSession.getMapper(MovieDao.class);

        //将所有影片和数量获取到并交给前端
        List<Movie> movieList = movieDao.queryList(startIndex, pageSize);

        int movieAmount = movieDao.count();
        int totalPage = (movieAmount + pageSize - 1) / pageSize;

        req.setAttribute("movieList", movieList);
        req.setAttribute("page", page);
        req.setAttribute("totalPage", totalPage);
        //movieList.forEach(movie -> System.out.println(movie));

        req.getRequestDispatcher("/moviepage").forward(req, resp);//moviepage展示页待完成
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //当用户根据年份，好评度进行搜索时。向servlet发送post请求
        //路径为/bigweb/movie
        String score = req.getParameter("score");//应该有4种情况null,good,normal,bad
        String time = req.getParameter("year");//3种情况：null,tmp,before

        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        MovieDao movieDao = sqlSession.getMapper(MovieDao.class);

        //根据分数分类
        Integer startScore, endScore;
        if (score.equals("good")){
            startScore = 8;
            endScore = 10;
        }else if (score.equals("normal")){
            startScore = 5;
            endScore = 7;
        }else if (score.equals("bad")){
            startScore = 0;
            endScore = 4;
        }else {
            startScore = null;
            endScore = null;
        }

        //根据时间分类
        Date startTime, endTime;
        DateUtil dateUtil = new DateUtil();
        if (time.equals("tmp")){
            startTime = dateUtil.getCurrentYearStartTime();
            endTime = dateUtil.getCurrentYearEndTime();
        }else if (time.equals("before")){
            endTime = dateUtil.getCurrentYearStartTime();
            startTime = dateUtil.getEarlyTime();
        }else {
            startTime = null;
            endTime = null;
        }

        List<Movie> movieList = movieDao.queryByScoreAndTime(startScore, endScore, startTime, endTime);
        Integer page = 1;
        Integer totalPage = 1;

        req.setAttribute("movieList", movieList);
        req.setAttribute("page", page);
        req.setAttribute("totalPage", totalPage);
        req.getRequestDispatcher("/moviepage").forward(req, resp);
    }
}
