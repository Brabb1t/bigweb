<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="<c:url value="/resources/changeMessage.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/remixicon.css"/>">
    <link rel="icon" type="image/x-icon" href="<c:url value="/resources/logo.png" />">

</head>
<body>
<h1>信息管理</h1>
<div id="maxbox">
    <h2>***信息修改***</h2>
    <div class="inputbox">
        <form method="post" action="/bigweb/meschange" id="changeMess">
            <div class="inputText">
                用户id：
                <input type="text" name="id" placeholder="id" id="changeId" value="${user.id}" readonly style="background:transparent;border:0"/><br>
                <span id="" class="error"> </span>
            </div>
            <div class="inputText">
                姓名修改：
                <input type="text" name="username" placeholder="姓名" id="changeUsername" value="${user.username}"/><br>
                <span id="e_username" class="error"> </span>
            </div>
            <div class="inputText">
                邮箱修改：
                <input type="text" name="email" placeholder="邮箱" id="changeEmail" value="${user.email}"/><br>
                <span id="e_email" class="error"> </span>
            </div>
            <div class="inputText">
                密码修改：
                <input type="password" name="password" placeholder="密码" id="changePassword"
                       value="${user.password}"/><br>
                <span id="e_password" class="error"> </span>
            </div>
            <%--修改信息还需要的参数，待美化--%>
            <%--            暂时先把id注释掉，感觉放在信息修改里面有点怪--%>
            <%--            <div class="inputText">--%>
            <%--                <input type="text" name="id" id="changeId"  value="${user.id}" readonly>--%>
            <%--            </div>--%>
            <div class="inputText">
                头像修改：
                <input type="text" name="header" required="required" id="changeHeader" value="${user.header}">
            </div>
            个人介绍：${user.description}
            <div class="inputText">
                <%--这里我把input改成了textarea，不知道会不会有什么影响，留意一下--%>
                <textarea rows="8" type="text" name="description" id="changeDescription" value="${user.description}">

                </textarea>
            </div>

            <input class="inputButton" type="submit" value="修改">
        </form>
    </div>
    <div>
        <br/>
        <%--用户自己的评论信息修改--%>
        <h2>***评论记录***</h2>
        <form method="post" action="/bigweb/cmtchange">
            <c:if test="${commentList.size()!=0}">
                <table border="1" >
                    <tr>
                        <td>评论编号</td>
                        <td>作品编号</td>
                        <td>评论时间</td>
                        <td>评论内容</td>
                        <td>评分</td>
                        <td>是否被禁言</td>
                        <td>操作</td>
                    </tr>
                    <c:if test="${commentList.size() > 0}">
                        <c:forEach var="i" begin="0" end="${commentList.size()-1}">
                            <tr>
                                <td>${commentList[i].id}</td>
                                <td>${commentList[i].itemId}</td>
                                <td>${commentList[i].commentTime}</td>
                                <td>${commentList[i].context}</td>
                                <td>${commentList[i].score}</td>
                                <td>${commentList[i].checked eq 1 ? "<p style='color:green'>通过</p>" : "<p style='color:red'>禁止</p>"}</td>
                                <td><a href="/bigweb/commentreceive?commentId=${commentList[i].id}&res=user">禁止该条评论</a></td>
                            </tr>
                        </c:forEach>
                    </c:if>

                </table>
            </c:if>
        </form>
    </div>
</div>
<script src="<c:url value="/resources/changeMessCheck.js"/>"></script>
</body>
</html>