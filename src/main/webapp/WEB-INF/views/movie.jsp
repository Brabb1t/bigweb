<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <meta name="referrer" content="no-referrer">
    <link rel="stylesheet" href="<c:url value="/resources/mmbdisplay.css" />">
    <link rel="icon" type="image/x-icon" href="<c:url value="/resources/icon.png" />">
</head>
<body>

<div>
    <%--根据电影的年份和星级筛选电影--%>
    <form method="post" action="/bigweb/movie">
        <div style="float: left">电影评分:</div>
        <div class="select"><input type="radio" name="score" value="null" checked><span class="b-radio"></span>不限</div>
        <div class="select"><input type="radio" name="score" value="good">高分</div>
        <div class="select"><input type="radio" name="score" value="normal">中等</div>
        <div class="select"><input type="radio" name="score" value="bad">低分</div><br><br>
        <hr style="width: 41%;float: left"><br>
        <div style="float: left">上映时间:</div>
        <div class="select"><input type="radio" name="year" value="null" checked>不限</div>
        <div class="select"><input type="radio" name="year" value="tmp">今年</div>
        <div class="select"><input type="radio" name="year" value="before">往年</div>
        <input type="submit" value="筛选" class="screen"><br><br>
        <hr style="width: 41%;float: left"><br>

    </form>
</div>

<%--展示--%>
<div class="itemdisplay" style="width: auto;height: auto;">
    <c:if test="${movieList.size() > 0}">
        <c:forEach var="i" begin="0" end="${movieList.size()-1}">
            <div class="moviedisplay">
                <a href="/bigweb/detail?id=${movieList[i].id}&cPage=1&cPageSize=10" target="hright">
                    <img src="${movieList[i].mainPic}">
                        <%--电影介绍 --%>
                    <div class="moviedes">
                        <h3>${movieList[i].name}</h3>
                        <i>导演： ${movieList[i].director}</i><br>
                        <i>主演： ${movieList[i].actor}、${movieList[i].actress}</i><br>
                        <i>上映时间： ${movieList[i].releaseDate}</i><br>
                        <font>简介： ${movieList[i].description}</font><br>
                        <em>评分： ${movieList[i].score}</em>
                    </div>
                </a>
            </div>
            <hr>
        </c:forEach>
    </c:if>

</div>

<div class="switch">
    <c:if test="${page > 1}">
        <a href="?page=${page-1}&pageSize=10">上一页</a>
    </c:if>
    <c:if test="${page < totalPage}">
        <a href="?page=${page+1}&pageSize=10">下一页</a>
    </c:if>
    当前页:${page}/共有:${totalPage}
</div>

</body>
</html>
