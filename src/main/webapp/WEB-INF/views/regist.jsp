<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/regist.css"/>" >
    <link rel="stylesheet" href="<c:url value="/resources/remixicon.css"/>">
    <link rel="icon" type="image/x-icon" href="<c:url value="/resources/icon.png" />">
</head>

<body>

  <div id="mmbox">
      <div id="leftbox" >
          <div id="leftlogo" style="width: 500px;height: 300px">
              <img src="resources/logo.png" style="width: 100%;height: 100%">
          </div>
      </div>
      <div id="maxbox">
          <h1>REGIST</h1>
          <div class="inputbox">
              <form method="post" action="/bigweb/regist" id="form">
                  <div class="inputText">
                      <i class="ri-account-circle-line"></i>
                      <input type="text" name="username" placeholder="姓名" id="username"/><br>
                      <span id="e_username" class="error"> </span>
                  </div>
                  <div class="inputText">
                      <i class="ri-mail-line"></i>
                      <input type="text" name="email" placeholder="邮箱" id="email"/><br>
                      <span id="e_email" class="error"> </span>
                  </div>
                  <div class="inputText">
                      <i class="ri-lock-line"></i>
                      <input type="password" name="password" placeholder="密码"id="password" /><br>
                      <span id="e_password" class="error"> </span>
                  </div>
                  <input class="inputButton" type="submit" value="regist">
              </form>

              <div id="login">
                    已经有账号了吗 ? <a href="<c:url value="/loginpage"/>">立即登录</a>
              </div>

          </div>

      </div>
  </div>
  <script src="<c:url value="/resources/registcheck.js"/>"></script>

</body>

</html>
