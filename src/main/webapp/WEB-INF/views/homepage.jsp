<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- 导入自定义标签 --%>
<%@ taglib prefix="mytag" uri="/WEB-INF/tld/MyTag.tld" %>
<html>
<head>
    <title>MMB</title>
    <meta name="referrer" content="no-referrer">
    <link rel="stylesheet" href="<c:url value="/resources/homepage.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/remixicon.css"/>">
    <link rel="icon" type="image/x-icon" href="<c:url value="/resources/icon.png" />">
</head>

<%--
    Book,Movie,Music三种对象每个传入一个 名字分别为 oneBook,oneMovie,oneMusic .
    <c:out value="${oneBook.name}"/>
--%>
<%-- 实例 输出书名 --%>
<body>
<%-- 主页头部--%>
<div class="homehead"
     style="width: 100%;height: 10%;background-image: linear-gradient(135deg, #fdfcfb 0%, #e2d1c3 100%);">
    <div id="logo"><img src="resources/logo.png"></div>
    <a href="/bigweb/meschange" target="hright">
        <c:if test="${curUser != null}">
            <div id="profile"><img src="${curUser.header}">${curUser.username}</div>
        </c:if>
        <c:if test="${curUser eq null}">
            <div id="profile"><a href="/bigweb/loginpage">你还没有登录！</a></div>
        </c:if>
    </a>
</div>
<%-- 主页左部--%>
<div class="homeleft"
     style="width: 10%;height:90%;background-image: linear-gradient(to top, #d299c2 0%, #fef9d7 100%);float: left;">
    <div class="selectbox">
        <div class="selectone">
            <a href="latest" target="hright"><i class="ri-home-heart-line"></i>&nbsp;首页</a>
        </div>

        <div class="selectone">
            <a href="/bigweb/movie?page=1&pageSize=10" target="hright" style="margin-top: 10%"> <i
                    class="ri-movie-2-line"></i>&nbsp;电影</a>
        </div>
        <div class="selectone">

            <a href="/bigweb/music?page=1&pageSize=10" target="hright"><i class="ri-disc-line"></i>&nbsp;音乐</a>
        </div>
        <div class="selectone">
            <a href="/bigweb/book?page=1&pageSize=10" target="hright"><i class="ri-book-line"></i>&nbsp;书籍</a>
        </div>

    </div>
</div>
<%--主显示页面--%>
<iframe name="hright" src="latest" style="width:85%;height: 80%" frameborder="0" align="right">

</iframe>
<%--页脚--%>
<div class="footer">
    <div class="footera">
        <ul>
            <li class="footli"><abbr title="西北工业大学软件学院19级">关于我们</abbr></li>
            <li class="footli"><abbr title="QQ:934697958">加入我们</abbr></li>
            <li class="footli"><abbr title="QQ:934697958">建议反馈</abbr></li>
            <li class="footli">浏览次数：<mytag:countertag/></li>
        </ul>
        <ul>
            <li class="copyr">CopyRight @2021 by bigweb</li>
        </ul>
    </div>
</div>

</body>

</html>
