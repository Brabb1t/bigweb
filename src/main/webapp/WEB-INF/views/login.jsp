<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>登录界面</title>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/login.css"/>" >
    <link rel="stylesheet" href="<c:url value="/resources/remixicon.css"/>">
    <link rel="icon" type="image/x-icon" href="<c:url value="/resources/icon.png" />">
</head>
<body>
    <div id="headbox">
        <h3>书影音系统</h3>
    </div>
    <div id="maxbox">
        <!--选择登陆方式-->
        <div class="login-header" onclick="checkRadio()">
            <input type="radio" name="login-option" id="username" checked="checked">
            <label for="username" class="u-btn">用户登录</label>
            <input type="radio" name="login-option" id="manager" >
            <label for="manager" class="m-btn">用户注册</label>
        </div>

       <!--登录表单-->
       <div class="inputbox">
           <div id="form-bar">
               <!--用户登录表单页-->
               <form method="post" action="/bigweb/login">
                   <div class="inputText">
                       <i class="ri-account-circle-line"></i>
                       <input type="text" name="username" placeholder="账 号"/>
                   </div>
                   <br>
                   <div class="inputText">
                       <i class="ri-lock-line"></i>
                       <input type="password" name="password" placeholder="密 码">
                   </div>
                   <br>
                   <a href="/bigweb/home" style="color: #accbee">我才不登录呢</a>
                   <br>
                   <input class="inputButton" type="submit" value="登录" id="loginbutton">

               </form>

               <!--注册表单-->
               <form method="post" action="/bigweb/regist" id="registform">
                   <div class="inputText">
                       <i class="ri-account-circle-line"></i>
                       <input type="text" name="username" placeholder="账号" id="registusername"/><br>
                       <span id="e_username" class="error"> </span>
                   </div>
                   <div class="inputText">
                       <i class="ri-mail-line"></i>
                       <input type="text" name="email" placeholder="邮箱" id="registemail"/><br>
                       <span id="e_email" class="error"> </span>
                   </div>
                   <div class="inputText">
                       <i class="ri-lock-line"></i>
                       <input type="password" name="password" placeholder="密 码" id="registpassword"><br>
                       <span id="e_password" class="error"> </span>
                   </div>
                   <input class="inputButton" type="submit" value="注册">
               </form>
           </div>
       </div>
   </div>
    <script src="<c:url value="/resources/script.js"/>"></script>
    <script src="<c:url value="/resources/registcheck.js"/>"></script>
</body>
</html>
