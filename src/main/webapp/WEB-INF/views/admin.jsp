<%--
    老师要求如下:
    管理员页面要实现的功能:
        1. 登录系统的管理员能够维护用户信息，对用户信息进行增、删、改、查。
        2. 维护书籍、电影、音乐信息，对书籍、电影、音乐信息进行增、删、改、查。
        3. 维护用户的评价信息，并可以查询指定用户的评价记录。
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>管理员页面</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="<c:url value="/resources/icon.png" />">

    <%--本地资源--%>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/admin.css"/>">
    <script type="text/javascript" src="<c:url value="/resources/admin.js"/>"></script>

</head>

<body>

<!-- 创建滚动监听 -->
<nav class="navbar navbar-expand-sm bg-body sticky-top p-0">
    <ul class="nav nav-tabs w-50" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" href="#user" data-mdb-toggle="tab" role="tab" aria-controls="user"
               aria-selected="true">用户</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" href="#movie" data-mdb-toggle="tab" role="tab" aria-controls="movie"
               aria-selected="false">电影</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" href="#music" data-mdb-toggle="tab" role="tab" aria-controls="music"
               aria-selected="false">音乐</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" href="#book" data-mdb-toggle="tab" role="tab" aria-controls="book"
               aria-selected="false">书籍</a>
        </li>
    </ul>
    <p class="m-0 fs-3">管理员维护</p>
</nav>

<div class="tab-content">
    <%--用户--%>
    <div id="user" class="tab-pane mt-3 fade show active" role="tabpanel">

        <div class="container-fluid">
            <legend>维护用户</legend>

            <div class="container-bar">
                <form method="post" name="form" action="/bigweb/adminop">
                    <div class="row mb-4">
                        <div class="col">
                            <div class="form-outline">
                                <input type="number" class="form-control" min="1" max="1000" name="id" id="form_id"
                                       placeholder="用户ID">
                                <label class="form-label" for="form_id">用户ID</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="username" id="form_username"
                                       placeholder="用户名">
                                <label class="form-label" for="form_username">用户名</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="email" id="form_email" placeholder="邮箱">
                                <label class="form-label" for="form_email">邮箱</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="password" class="form-control" name="password" id="form_password"
                                       placeholder="密码">
                                <label class="form-label" for="form_password">密码</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="header" id="form_header" placeholder="头像">
                                <label class="form-label" for="form_header">头像</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="description" id="form_description"
                                       placeholder="描述">
                                <label class="form-label" for="form_description">描述</label>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="addUser" value="addUser"/>
                        <label class="form-check-label" for="addUser">添加用户</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryByIdUser"
                               value="queryByIdUser"/>
                        <label class="form-check-label" for="queryByIdUser">查询(按照用户Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryByNameUser"
                               value="queryByNameUser"/>
                        <label class="form-check-label" for="queryByNameUser">查询(按照用户名)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="deleteUser"
                               value="deleteUser"/>
                        <label class="form-check-label" for="deleteUser">删除(按照用户Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="updateUser"
                               value="updateUser"/>
                        <label class="form-check-label" for="updateUser">更改(按照用户Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryAllUser"
                               value="queryAllUser"/>
                        <label class="form-check-label" for="queryAllUser">查询全部</label>
                    </div>

                    <br><br>
                    <input type="submit" class="btn btn-primary btn-block" value="确认">
                </form>
            </div>
            <%--查询信息--%>
            <h3>查询信息:</h3>
            <table class="table table-bordered">
                <tr>
                    <th>用户id</th>
                    <th>用户名</th>
                    <th>邮箱</th>
                    <th>密码</th>
                    <th>头像</th>
                    <th>描述</th>
                </tr>
                <c:if test="${user!=null}">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.username}</td>
                        <td>${user.email}</td>
                        <td>${user.password}</td>
                        <td>${user.header}</td>
                        <td>${user.description}</td>
                    </tr>
                </c:if>
            </table>

            <%--显示所有用户以及所有信息--%>
            <h3>全部信息:</h3>
            <table class="table table-bordered">
                <tr>
                    <th>用户id</th>
                    <th>用户名</th>
                    <th>邮箱</th>
                    <th>密码</th>
                    <th>头像</th>
                    <th>描述</th>
                </tr>

                <%--全部用户--%>
                <c:if test="${userList.size()>0}">
                    <c:forEach var="i" begin="0" end="${userList.size()-1}">
                        <tr>
                            <td>${userList[i].id}</td>
                            <td>${userList[i].username}</td>
                            <td>${userList[i].email}</td>
                            <td>${userList[i].password}</td>
                            <td>${userList[i].header}</td>
                            <td>${userList[i].description}</td>
                        </tr>
                    </c:forEach>
                </c:if>

            </table>

            <%--评论--%>
            <fieldset style="margin-top:20px">
                <legend>评论</legend>
                <%--该用户发表的所有评论--%>
                <%--查询到单个用户（根据用户id/用户名名）时显示该用户的所有评论信息--%>
                <h3>用户${user.username}的所有评论信息:</h3>
                <table class="table table-bordered">
                    <tr>
                        <td>评论编号</td>
                        <td>作品编号</td>
                        <td>评论时间</td>
                        <td>评论内容</td>
                        <td>评分</td>
                        <td>是否被禁言</td>
                        <td>操作</td>
                    </tr>
                    <c:if test="${commentList.size()>0}">
                        <c:forEach var="i" begin="0" end="${commentList.size()-1}">
                            <tr>
                                <td>${commentList[i].id}</td>
                                <td>${commentList[i].itemId}</td>
                                <td>${commentList[i].commentTime}</td>
                                <td>${commentList[i].context}</td>
                                <td>${commentList[i].score}</td>
                                <td>${commentList[i].checked eq 1 ? "<p style='color:green'>通过</p>" : "<p style='color:red'>禁止</p>"}</td>
                                <td><a href="/bigweb/commentreceive?commentId=${commentList[i].id}&res=admin">禁止该条评论</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                </table>
            </fieldset>
        </div>
    </div>

    <%--电影--%>
    <div id="movie" class="tab-pane mt-3 fade" role="tabpanel">

        <div class="container-fluid">
            <fieldset>
                <legend>维护电影</legend>
                <form method="post" name="form" action="/bigweb/adminop">

                    <div class="row mb-4">
                        <div class="col">
                            <div class="form-outline">
                                <input type="number" class="form-control" min="1" max="1000" name="movieid"
                                       id="form_movieid" placeholder="电影ID">
                                <label class="form-label" for="form_movieid">电影ID</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="moviename" id="form_moviename"
                                       placeholder="电影名">
                                <label class="form-label" for="form_moviename">电影名</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="director" id="form_director"
                                       placeholder="导演">
                                <label class="form-label" for="form_director">导演</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="actor" id="form_actor" placeholder="男演员">
                                <label class="form-label" for="form_password">男演员</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="actress" id="form_actress"
                                       placeholder="女演员">
                                <label class="form-label" for="form_header">女演员</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="date" class="form-control dropdown-menu-dark" value="2021/11/11"
                                       name="releaseDate" id="form_releaseDate1" placeholder="上映时间">
                                <label class="form-label" for="form_releaseDate1">上映时间</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="number" class="form-control" value="5.0" min="0" max="10" name="score"
                                       id="form_score1" placeholder="评分">
                                <label class="form-label" for="form_score1">评分</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="description" id="form_description1"
                                       placeholder="描述">
                                <label class="form-label" for="form_description1">描述</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="mainPic" id="form_mainPic1"
                                       placeholder="海报">
                                <label class="form-label" for="form_mainPic1">海报</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="describePic" id="form_describePic1"
                                       placeholder="详细海报">
                                <label class="form-label" for="form_describePic1">详细海报</label>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="addMovie" value="addMovie"/>
                        <label class="form-check-label" for="addMovie">添加电影</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryByIdMovie"
                               value="queryByIdMovie"/>
                        <label class="form-check-label" for="queryByIdMovie">查询(按照电影Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryByNameMovie"
                               value="queryByNameMovie"/>
                        <label class="form-check-label" for="queryByNameMovie">查询(按照电影名)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="deleteMovie"
                               value="deleteMovie"/>
                        <label class="form-check-label" for="deleteMovie">删除(按照电影Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="updateMovie"
                               value="updateMovie"/>
                        <label class="form-check-label" for="updateMovie">更改(按照电影Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryAllMovie"
                               value="queryAllMovie"/>
                        <label class="form-check-label" for="queryAllMovie">查询全部</label>
                    </div>

                    <br><br>
                    <input type="submit" class="btn btn-primary btn-block" value="确认">
                </form>

                <%--查询信息--%>
                <h3>查询信息:</h3>
                <table class="table table-bordered">
                    <tr>
                        <th>电影id</th>
                        <th>电影名</th>
                        <th>导演</th>
                        <th>男演员</th>
                        <th>女演员</th>
                        <th>上映时间</th>
                        <th>评分</th>
                        <th>描述</th>
                        <th>海报</th>
                        <th>详细海报</th>
                    </tr>
                    <c:if test="${movie!=null}">
                        <tr>
                            <td>${movie.id}</td>
                            <td>${movie.name}</td>
                            <td>${movie.director}</td>
                            <td>${movie.actor}</td>
                            <td>${movie.actress}</td>
                            <td>${movie.releaseDate}</td>
                            <td>${movie.score}</td>
                            <td>${movie.mainPic}</td>
                            <td>${movie.describePic}</td>
                        </tr>
                    </c:if>
                </table>

                <h3>全部信息:</h3>
                <table class="table table-bordered">
                    <tr>
                        <th>电影id</th>
                        <th>电影名</th>
                        <th>导演</th>
                        <th>男演员</th>
                        <th>女演员</th>
                        <th>上映时间</th>
                        <th>评分</th>
                        <th>描述</th>
                        <th>海报</th>
                        <th>详细海报</th>
                    </tr>

                    <%--全部电影--%>
                    <c:if test="${movieList.size()>0}">
                        <c:forEach var="i" begin="0" end="${movieList.size()-1}">
                            <tr>
                                <td>${movieList[i].id}</td>
                                <td>${movieList[i].name}</td>
                                <td>${movieList[i].director}</td>
                                <td>${movieList[i].actor}</td>
                                <td>${movieList[i].actress}</td>
                                <td>${movieList[i].releaseDate}</td>
                                <td>${movieList[i].score}</td>
                                <td>${movieList[i].mainPic}</td>
                                <td>${movieList[i].describePic}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                </table>

                <%--评论--%>
                <fieldset>
                    <legend>评论</legend>
                    <%--该电影相关的所有评论--%>
                    <%--查询到单个电影（根据电影id/电影名）时显示该电影的所有评论信息--%>
                    <h3>电影${movie.name}相关的所有评论</h3>
                    <table class="table table-bordered">
                        <tr>
                            <td>评论编号</td>
                            <td>作品编号</td>
                            <td>评论者</td>
                            <td>评论时间</td>
                            <td>评论内容</td>
                            <td>评分</td>
                            <td>是否被禁言</td>
                            <td>操作</td>
                        </tr>
                        <c:if test="${moviecommentList.size()>0}">
                            <c:forEach var="i" begin="0" end="${moviecommentList.size()-1}">
                                <tr>
                                    <td>${moviecommentList[i].id}</td>
                                    <td>${moviecommentList[i].itemId}</td>
                                    <td>${moviecommentList[i].userId}</td>
                                    <td>${moviecommentList[i].commentTime}</td>
                                    <td>${moviecommentList[i].context}</td>
                                    <td>${moviecommentList[i].score}</td>
                                    <td>${moviecommentList[i].checked eq 1 ? "<p style='color:green'>通过</p>" : "<p style='color:red'>禁止</p>"}</td>
                                    <td><a href="/bigweb/commentreceive?commentId=${moviecommentList[i].id}&res=admin">禁止该条评论</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                </fieldset>
            </fieldset>
        </div>
    </div>

    <%--音乐--%>
    <div id="music" class="tab-pane mt-3 fade" role="tabpanel">
        <div class="container-fluid">
            <fieldset>
                <legend>维护音乐</legend>
                <form method="post" name="form" action="/bigweb/adminop">

                    <div class="row md-4">
                        <div class="col">
                            <div class="form-outline">
                                <input type="number" class="form-control" min="1" max="1000" name="musicid"
                                       id="form_musicid" placeholder="音乐id">
                                <label class="form-label" for="form_musicid">音乐id</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="musicname" id="form_musicname"
                                       placeholder="音乐名">
                                <label class="form-label" for="form_musicname">音乐名</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="singer" id="form_singer" placeholder="歌手">
                                <label class="form-label" for="form_singer">歌手</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="album" id="form_album" placeholder="专辑">
                                <label class="form-label" for="form_album">专辑</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="date" class="form-control dropdown-menu-dark" value="2021/11/2"
                                       name="releaseDate" id="form_releaseDate2" placeholder="发行时间">
                                <label class="form-label" for="form_releaseDate2">发行时间</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="number" class="form-control" value="5.0" min="0" max="10" name="score"
                                       id="form_score2" placeholder="评分">
                                <label class="form-label" for="form_score2">评分</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="description" id="form_description2"
                                       placeholder="描述">
                                <label class="form-label" for="form_description2">描述</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="mainPic" id="form_mainPic2"
                                       placeholder="海报">
                                <label class="form-label" for="form_mainPic2">海报</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="describePic" id="form_describePic2"
                                       placeholder="详细海报">
                                <label class="form-label" for="form_describePic2">详细海报</label>
                            </div>
                        </div>
                    </div>

                    <br><br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="addMusic" value="addMusic"/>
                        <label class="form-check-label" for="addMusic">添加音乐</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryByIdMusic"
                               value="queryByIdMusic"/>
                        <label class="form-check-label" for="queryByIdMusic">查询(按照音乐Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryByNameMusic"
                               value="queryByNameMusic"/>
                        <label class="form-check-label" for="queryByNameMusic">查询(按照音乐名)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="deleteMusic"
                               value="deleteMusic"/>
                        <label class="form-check-label" for="deleteMusic">删除(按照音乐Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="updateMusic"
                               value="updateMusic"/>
                        <label class="form-check-label" for="updateMusic">更改(按照音乐Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryAllMusic"
                               value="queryAllMusic"/>
                        <label class="form-check-label" for="queryAllMusic">查询全部</label>
                    </div>

                    <br><br>
                    <input type="submit" class="btn btn-primary btn-block" value="确认">
                </form>

                <%--查询信息--%>
                <h3>查询信息:</h3>
                <table class="table table-bordered">
                    <tr>
                        <th>音乐id</th>
                        <th>音乐名</th>
                        <th>歌手</th>
                        <th>专辑</th>
                        <th>发行时间</th>
                        <th>评分</th>
                        <th>描述</th>
                        <th>海报</th>
                        <th>详细海报</th>
                    </tr>
                    <c:if test="${music!=null}">
                        <tr>
                            <td>${music.id}</td>
                            <td>${music.name}</td>
                            <td>${music.singer}</td>
                            <td>${music.album}</td>
                            <td>${music.releaseDate}</td>
                            <td>${music.score}</td>
                            <td>${music.mainPic}</td>
                            <td>${music.describePic}</td>
                        </tr>
                    </c:if>
                </table>

                <h3>全部信息:</h3>
                <table class="table table-bordered">
                    <tr>
                        <th>音乐id</th>
                        <th>音乐名</th>
                        <th>歌手</th>
                        <th>专辑</th>
                        <th>发行时间</th>
                        <th>评分</th>
                        <th>描述</th>
                        <th>海报</th>
                        <th>详细海报</th>
                    </tr>

                    <%--全部音乐--%>
                    <c:if test="${musicList.size()>0}">
                        <c:forEach var="i" begin="0" end="${musicList.size()-1}">
                            <tr>
                                <td>${musicList[i].id}</td>
                                <td>${musicList[i].name}</td>
                                <td>${musicList[i].singer}</td>
                                <td>${musicList[i].album}</td>
                                <td>${musicList[i].releaseDate}</td>
                                <td>${musicList[i].score}</td>
                                <td>${musicList[i].mainPic}</td>
                                <td>${musicList[i].describePic}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                </table>

                <%--评论--%>
                <fieldset>
                    <legend>评论</legend>
                    <%--该音乐相关的所有评论--%>
                    <%--查询到单个音乐（根据音乐id/音乐名）时显示该音乐的所有评论信息--%>
                    <h3>音乐${music.name}相关的所有评论</h3>
                    <table class="table table-bordered">
                        <tr>
                            <td>评论编号</td>
                            <td>作品编号</td>
                            <td>评论者</td>
                            <td>评论时间</td>
                            <td>评论内容</td>
                            <td>评分</td>
                            <td>是否被禁言</td>
                            <td>操作</td>
                        </tr>
                        <c:if test="${musiccommentList.size()>0}">
                            <c:forEach var="i" begin="0" end="${musiccommentList.size()-1}">
                                <tr>
                                    <td>${musiccommentList[i].id}</td>
                                    <td>${musiccommentList[i].itemId}</td>
                                    <td>${musiccommentList[i].userId}</td>
                                    <td>${musiccommentList[i].commentTime}</td>
                                    <td>${musiccommentList[i].context}</td>
                                    <td>${musiccommentList[i].score}</td>
                                    <td>${musiccommentList[i].checked eq 1 ? "<p style='color:green'>通过</p>" : "<p style='color:red'>禁止</p>"}</td>
                                    <td><a href="/bigweb/commentreceive?commentId=${musiccommentList[i].id}&res=admin">禁止该条评论</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                </fieldset>
            </fieldset>
        </div>
    </div>

    <%--书籍--%>
    <div id="book" class="tab-pane mt-3 fade" role="tabpanel">

        <div class="container-fluid">
            <fieldset>
                <legend>维护书籍</legend>
                <form method="post" name="form" action="/bigweb/adminop">

                    <div class="row md-4">
                        <div class="col">
                            <div class="form-outline">
                                <input type="number" class="form-control" min="1" max="1000" name="bookid"
                                       id="form_bookid" placeholder="书籍id">
                                <label class="form-label" for="form_bookid">书籍id</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="bookname" id="form_bookname"
                                       placeholder="书名">
                                <label class="form-label" for="form_bookname">书名</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="author" id="form_author" placeholder="作者">
                                <label class="form-label" for="form_author">作者</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="publisher" id="form_publisher"
                                       placeholder="出版社">
                                <label class="form-label" for="form_publisher">出版社</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="date" class="form-control dropdown-menu-dark" value="2021/11/2"
                                       name="releaseDate" id="form_releaseDate3" placeholder="出版时间">
                                <label class="form-label" for="form_releaseDate3">出版时间</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="number" class="form-control" value="5.0" min="0" max="10" name="score"
                                       id="form_score3" placeholder="评分">
                                <label class="form-label" for="form_score3">评分</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="description" name=""
                                       id="form_description3" placeholder="描述">
                                <label class="form-label" for="form_description3">描述</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="mainPic" id="form_mainPic3"
                                       placeholder="海报">
                                <label class="form-label" for="form_mainPic3">海报</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input type="text" class="form-control" name="describePic" id="form_describePic3"
                                       placeholder="详细海报">
                                <label class="form-label" for="form_describePic3">详细海报</label>
                            </div>
                        </div>
                    </div>

                    <br><br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="addBook" value="addBook"/>
                        <label class="form-check-label" for="addBook">添加书籍</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryByIdBook"
                               value="queryByIdBook"/>
                        <label class="form-check-label" for="queryByIdBook">查询(按照书籍Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryByNameBook"
                               value="queryByNameBook"/>
                        <label class="form-check-label" for="queryByNameBook">查询(按照书籍名)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="deleteBook"
                               value="deleteBook"/>
                        <label class="form-check-label" for="deleteBook">删除(按照书籍Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="updateBook"
                               value="updateBook"/>
                        <label class="form-check-label" for="updateBook">更改(按照书籍Id)</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="operation" id="queryAllBook"
                               value="queryAllBook"/>
                        <label class="form-check-label" for="queryAllBook">查询全部</label>
                    </div>

                    <br><br>
                    <input type="submit" class="btn btn-primary btn-block" value="确认">
                </form>

                <%--查询信息--%>
                <h3>查询信息:</h3>
                <table class="table table-bordered">
                    <tr>
                        <th>书籍id</th>
                        <th>书名</th>
                        <th>作者</th>
                        <th>出版社</th>
                        <th>出版时间</th>
                        <th>评分</th>
                        <th>描述</th>
                        <th>海报</th>
                        <th>详细海报</th>
                    </tr>
                    <c:if test="${book!=null}">
                        <tr>
                            <td>${book.id}</td>
                            <td>${book.name}</td>
                            <td>${book.author}</td>
                            <td>${book.publisher}</td>
                            <td>${book.releaseDate}</td>
                            <td>${book.score}</td>
                            <td>${book.mainPic}</td>
                            <td>${book.describePic}</td>

                        </tr>
                    </c:if>
                </table>

                <h3>全部信息:</h3>
                <table class="table table-bordered">
                    <tr>
                        <th>书籍id</th>
                        <th>书名</th>
                        <th>作者</th>
                        <th>出版社</th>
                        <th>出版时间</th>
                        <th>评分</th>
                        <th>描述</th>
                        <th>海报</th>
                        <th>详细海报</th>
                    </tr>

                    <%--全部书籍--%>
                    <c:if test="${bookList.size()>0}">
                        <c:forEach var="i" begin="0" end="${bookList.size()-1}">
                            <tr>
                                <td>${bookList[i].id}</td>
                                <td>${bookList[i].name}</td>
                                <td>${bookList[i].author}</td>
                                <td>${bookList[i].publisher}</td>
                                <td>${bookList[i].releaseDate}</td>
                                <td>${bookList[i].score}</td>
                                <td>${bookList[i].description}</td>
                                <td>${bookList[i].mainPic}</td>
                                <td>${bookList[i].describePic}</td>
                            </tr>
                        </c:forEach>
                    </c:if>
                </table>

                <%--评论--%>
                <fieldset>
                    <legend>评论</legend>
                    <%--该书籍相关的所有评论--%>
                    <%--查询到单个书籍（根据书籍id/书名）时显示该书的所有评论信息--%>
                    <h3>书籍${book.name}相关的所有评论</h3>
                    <table class="table table-bordered">
                        <tr>
                            <td>评论编号</td>
                            <td>作品编号</td>
                            <td>评论者</td>
                            <td>评论时间</td>
                            <td>评论内容</td>
                            <td>评分</td>
                            <td>是否被禁言</td>
                            <td>操作</td>
                        </tr>
                        <c:if test="${bookcommentList.size()>0}">
                            <c:forEach var="i" begin="0" end="${bookcommentList.size()-1}">
                                <tr>
                                    <td>${bookcommentList[i].id}</td>
                                    <td>${bookcommentList[i].itemId}</td>
                                    <td>${bookcommentList[i].userId}</td>
                                    <td>${bookcommentList[i].commentTime}</td>
                                    <td>${bookcommentList[i].context}</td>
                                    <td>${bookcommentList[i].score}</td>
                                    <td>${bookcommentList[i].checked eq 1 ? "<p style='color:green'>通过</p>" : "<p style='color:red'>禁止</p>"}</td>
                                    <td><a href="/bigweb/commentreceive?commentId=${bookcommentList[i].id}&res=admin">禁止该条评论</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                </fieldset>
            </fieldset>
        </div>
    </div>
    <%--解决input标签不显示边框的问题--%>
    <script>
        document.querySelectorAll('.form-outline').forEach(outline => new mdb.Input(outline).init());
    </script>

</div>
</body>
</html>
