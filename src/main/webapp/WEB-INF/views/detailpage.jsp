<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>MMB</title>
    <meta name="referrer" content="no-referrer">
    <link rel="icon" type="image/x-icon" href="<c:url value="/resources/logo.png" />">
    <style type="text/css">
        .addcomment{
            margin-left: 30%;
            width: 50%;
            height: 200px;
        }
        #addframe{
            width: 300px;
            height: 60px;
            margin-left: 20%;
        }
        #addbutton{
            width: 180px;
            height: 30px;
            border-radius: 25px;
            background-image: linear-gradient(-20deg, #e9defa 0%, #fbfcdb 100%);
            margin-left: 30%;
        }
        #addbutton:hover{
            background-image: linear-gradient(to top, #accbee 0%, #e7f0fd 100%);
        }
        .comment i{
            font-style: normal;
        }
    </style>
</head>
<body>
   <div class="display" style="width: 100%;height: 30%;">
      <c:if test="${chosenMovie!=null}">
          <img src="${chosenMovie.getDescribePic()}" style="width: 12%;height: 100%;float: left;">
          <div class="moviedes" style="margin-left: 300px;">
              <h3>${chosenMovie.name}</h3>
              <i>导演： ${chosenMovie.director}</i><br>
              <i>主演： ${chosenMovie.actor}、${chosenMovie.actress}</i><br>
              <i>上映时间： ${chosenMovie.releaseDate}</i><br>
              <font>简介： ${chosenMovie.description}</font><br>
              <em>评分： ${chosenMovie.score}</em>
          </div>
      </c:if>
       <c:if test="${chosenMusic!=null}">
           <img src="${chosenMusic.getDescribePic()}" style="width: 19%;height: 100%;float: left;">
           <div class="musicdes" style="margin-left: 400px;">
               <h3>${chosenMusic.name}</h3>
               <i>发行时间： ${chosenMusic.releaseDate}</i><br>
               <i>歌手： ${chosenMusic.singer}</i><br>
               <i>唱片： ${chosenMusic.album}</i><br>
               <font>简介： ${chosenMusic.description}</font><br>
               <em>评分： ${chosenMusic.score}</em>
           </div>
       </c:if>
       <c:if test="${chosenBook!=null}">
           <img src="${chosenBook.getDescribePic()}" style="width: 12%;height: 100%;float: left;">
           <div class="bookdes" style="margin-left: 300px;">
               <h3>${chosenBook.name}</h3>
               <i>出版时间： ${chosenBook.releaseDate}</i><br>
               <i>作者： ${chosenBook.author}</i><br>
               <i>出版商： ${chosenBook.publisher}</i><br>
               <font>简介： ${chosenBook.description}</font><br>
               <em>评分： ${chosenBook.score}</em>
           </div>
       </c:if>

   </div>
   <div class="comment" style="margin-top: 30px;">
       <c:if test="${cuList.size()!=0}">
           <c:forEach var="i" begin="0" end="${cuList.size()-1}">
               <div style="width: 100%;">
                   <i>${cuList[i].context} </i><br>
                   <i>评论者： ${cuList[i].userName} </i>
               </div>
               <hr>
           </c:forEach>
       </c:if>

       <div class="switch" style="text-align: center">
           <c:if test="${cPage > 1}">
               <a href="?id=${id}&cPage=${cPage-1}&cPageSize=10">上一页</a>
           </c:if>
           <c:if test="${cPage < cTotalPage}">
               <a href="?id=${id}&cPage=${cPage+1}&cPageSize=10">下一页</a>
           </c:if>
           当前评论页:${cPage}/共有:${cTotalPage}
       </div><br>

   </div>
   <div class="addcomment">
       <form method="post" action="/bigweb/commentreceive">
           <div><i>作品编号：</i><input name="itemId" value="${id}" readonly style="background:transparent;border:0"></div>
           <br>
           <div><i>评分(整数：1-10)</i>：<input type="number" name="score" min="1" max="10" value="5" style="margin-left: 8%;"></div>
           <br>
           <div><i>评论：</i><textarea name="context" id="addframe"></textarea><br></div>
           <br>
           <input class="inputButton" type="submit" value="提交评论" id="addbutton">
       </form>
   </div>
</body>
</html>
