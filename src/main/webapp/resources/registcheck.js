window.onload=function (){
    document.getElementById("registform").onsubmit=function (){
        return checkUsername() && checkPassword() && checkEmail();
    }
    document.getElementById("registusername").onblur = checkUsername;
    document.getElementById("registpassword").onblur = checkPassword;
    document.getElementById("registemail").onblur = checkEmail
}

function checkUsername(){
    var username=document.getElementById("registusername").value;
    var reg_username = /^([a-zA-Z0-9_-])/;
    var flag = reg_username.test(username);

    var e_username=document.getElementById("e_username");
    if(flag){
        e_username.innerHTML='<span style="color: greenyellow">格式正确</span>';

    }else{
        e_username.innerHTML='<span style="color: red">用户名格式错误，请重新输入</span>';
    }
    return flag;

}
function checkPassword(){
    var password=document.getElementById("registpassword").value;

    var reg_password=/^\w{6,12}$/;
    var flag=reg_password.test(password);
    var e_password=document.getElementById("e_password");

    if(flag){

        e_password.innerHTML='<span style="color: #27ae60">格式正确</span>';
    }else {
        e_password.innerHTML='<span style="color: red">密码格式错误，请输入6-12位</span>';
    }
    return flag;
}

function checkEmail(){
    var email=document.getElementById("registemail").value;
    var reg_email=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;
    var flag=reg_email.test(email);
    var  e_email=document.getElementById("e_email");

    if(flag){

        e_email.innerHTML='<span style="color: #27ae60">格式正确</span>';

    }else {
        e_email.innerHTML='<span style="color: red">邮箱格式错误，请重新输入</span>';
    }
    return flag;

}