window.onload = function () {
    document.getElementById("changeMess").onsubmit = function () {
        return checkUsername() && checkPassword() && checkEmail();
    }
    document.getElementById("changeUsername").onblur = checkUsername;
    document.getElementById("changePassword").onblur = checkPassword;
    document.getElementById("changeEmail").onblur = checkEmail
}

function checkUsername() {
    let username = document.getElementById("changeUsername").value;
    let reg_username = /^([a-zA-Z0-9_-])/;
    let flag = reg_username.test(username);

    let e_username = document.getElementById("e_username");
    if (flag) {
        e_username.innerHTML = '<span style="color: greenyellow">格式正确</span>';

    } else {
        e_username.innerHTML = '<span style="color: red">用户名格式错误，请重新输入</span>';
    }
    return flag;

}

function checkPassword() {
    let password = document.getElementById("changePassword").value;

    let reg_password = /^\w{6,12}$/;
    let flag = reg_password.test(password);
    let e_password = document.getElementById("e_password");

    if (flag) {

        e_password.innerHTML = '<span style="color: #27ae60">格式正确</span>';
    } else {
        e_password.innerHTML = '<span style="color: red">密码格式错误，请输入6-12位</span>';
    }
    return flag;
}

function checkEmail() {
    let email = document.getElementById("changeEmail").value;
    let reg_email = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;
    let flag = reg_email.test(email);
    let e_email = document.getElementById("e_email");

    if (flag) {

        e_email.innerHTML = '<span style="color: #27ae60">格式正确</span>';

    } else {
        e_email.innerHTML = '<span style="color: red">邮箱格式错误，请重新输入</span>';
    }
    return flag;

}